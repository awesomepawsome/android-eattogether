<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

// check for post data
if (isset($_GET["email"])) {
    $email = $_GET['email'];
        
    $query = "select idRestaurant, restaurantName, type, website, phone, address, 
    				operatingHour, photo, nTable
				from restaurant
				where Owner_Login_email = '$email'
				limit 1";

    // get a user profile from customer table
    $result = mysql_query($query);

    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {

            $result = mysql_fetch_array($result);

            $restaurantProfile = array();
            $restaurantProfile["idRestaurant"] = $result["idRestaurant"];
            $restaurantProfile["restaurantName"] = $result["restaurantName"];
            $restaurantProfile["type"] = $result["type"];
            $restaurantProfile["website"] = $result["website"];
            $restaurantProfile["phone"] = $result["phone"];
            $restaurantProfile["address"] = $result["address"];
            $restaurantProfile["operatingHour"] = $result["operatingHour"];
            $restaurantProfile["phone"] = $result["phone"];
            $restaurantProfile["nTable"] = $result["nTable"];
            // success
            $response["success"] = 1;

            // user node
            $response["profile"] = array();

            array_push($response["profile"], $restaurantProfile);

            // echoing JSON response
			echo json_encode(utf8ize($response));
			
        } else {
            $response = resultNotFoundMsg();
            echo json_encode($response);
            
        }
    } else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}

function utf8ize($data) {
    if (is_array($data)) {
        foreach ($data as $k => $v) {
            $data[$k] = utf8ize($v);
        }
    } else if (is_string ($data)) {
        return utf8_encode($data);
    }
    return $data;
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No user found";
    
    return $msgArray;
}

?>