<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

// check for post data
if (isset($_GET["email"]) && isset($_GET["password"])) {
    $email = $_GET['email'];
    $password = $_GET['password'];
    
    $query = "select o.firstName, o.lastName, o.phone, l.email
				from owner o
				inner join login l
				on o.login_email = l.email
				where l.email = '$email'
				and l.password = '$password'
				limit 1";

    // get a user profile from customer table
    $result = mysql_query($query);

    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {

            $result = mysql_fetch_array($result);

            $profile = array();
            $profile["firstName"] = $result["firstName"];
            $profile["lastName"] = $result["lastName"];
            $profile["phone"] = $result["phone"];
            $profile["email"] = $result["email"];
            // success
            $response["success"] = 1;

            // user node
            $response["profile"] = array();

            array_push($response["profile"], $profile);

            // echoing JSON response
            echo json_encode($response);
        } else {
            $response = userNotFoundMsg();
            echo json_encode($response);
        }
    } else {
    	$response = userNotFoundMsg();
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}

function userNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No user found";
    
    return $msgArray;
}

?>