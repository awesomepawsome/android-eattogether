<?php

// array for JSON response
$response = array();

// check for required fields
$isFoodNameSet = isset($_POST['foodName']) && !empty($_POST['foodName']);
$isPriceSet = isset($_POST['price']) && !empty($_POST['price']);
$isPhotoSet = isset($_POST['photo']) && !empty($_POST['photo']);
$isIdRestaurantSet = isset($_POST['idRestaurant']) && !empty($_POST['idRestaurant']);

if ($isFoodNameSet && $isPriceSet && $isIdRestaurantSet) {
    
    $foodName = $_POST['foodName'];
    $price = $_POST['price'];
    $idRestaurant = $_POST['idRestaurant'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    
    if ($isPhotoSet){
	    $photo = $_POST['photo'];
       	$query = "insert into food (foodName, price, photo, Restaurant_idRestaurant)
			select * from (select '$foodName' , '$price', '$photo', '$idRestaurant') as temp
			where not exists(
				select foodName from food where foodName = '$foodName' 
			)";
    }else{
       	$query = "insert into food (foodName, price, Restaurant_idRestaurant)
			select * from (select '$foodName' , '$price', '$idRestaurant') as temp
			where not exists(
				select foodName from food where foodName = '$foodName' 
			)";    
    }

    // mysql inserting a new row
    $result = mysql_query($query);

    // check if row inserted or not
    if ($result) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Food successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! An error occurred.";
        
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>