<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

if (isset($_GET["idRestaurant"])) {
	$idRestaurant = $_GET['idRestaurant'];
    
    $query = "SELECT r.idReservation, r.numberOfPax, DATE(r.reserveDateTime) AS reserve, DATE_FORMAT(r.reserveDateTime, '%H:%i') AS time, c.displayName, c.phone, c.Login_email
				FROM reservation r
				INNER JOIN customer c
				ON r.Customer_organizer = c.Login_email
				WHERE DATE(r.reserveDateTime) = CURDATE()
				AND Restaurant_idRestaurant = '$idRestaurant'
				ORDER BY reserveDateTime";

	$result = mysql_query($query) or die(mysql_error());

	// check for empty result
	if (mysql_num_rows($result) > 0) {
    	// looping through all results
    	$response["results"] = array();
    
    	while ($row = mysql_fetch_array($result)) {

        	$reservation = array();
            $reservation["idReservation"] = $row["idReservation"];
            $reservation["numberOfPax"] = $row["numberOfPax"];
            $reservation["reserve"] = $row["reserve"];
			$reservation["time"] = $row["time"];
            $reservation["displayName"] = $row["displayName"];
            $reservation["phone"] = $row["phone"];
            $reservation["Login_email"] = $row["Login_email"];

        	array_push($response["results"], $reservation);
    	}
    
    	// success
    	$response["success"] = 1;
    	// echoing JSON response
    	echo json_encode(utf8ize($response));
    
	} else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
	}

}else{
	// required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);

}

function utf8ize($data) {
    if (is_array($data)) {
        foreach ($data as $k => $v) {
            $data[$k] = utf8ize($v);
        }
    } else if (is_string ($data)) {
        return utf8_encode($data);
    }
    return $data;
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No result found";
    
    return $msgArray;
}

?>