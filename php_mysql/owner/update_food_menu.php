<?php

// array for JSON response
$response = array();

// check for required fields
$isIdFoodSet = isset($_POST['idFood']) && !empty($_POST['idFood']);
$isFoodNameSet = isset($_POST['foodName']) && !empty($_POST['foodName']);
$isPriceSet = isset($_POST['price']) && !empty($_POST['price']);
$isPhotoSet = isset($_POST['photo']) && !empty($_POST['photo']);

// $isAllFieldsSet = isset($_POST['foodName']) && isset($_POST['price']) && isset($_POST['photo']) && isset($_POST['idFood']);

if ($isIdFoodSet && $isFoodNameSet && $isPriceSet) {
    
	$idFood = $_POST['idFood'];
    $foodName = $_POST['foodName'];
    $price = $_POST['price'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    
	if ($isPhotoSet) {
		$photo = $_POST['photo'];
		$query = "UPDATE food
					SET foodName = '$foodName', price = '$price', photo = '$photo'
					WHERE idFood = '$idFood'";
	} else {
		$query = "UPDATE food
					SET foodName = '$foodName', price = '$price'
					WHERE idFood = '$idFood'";
	}

    // mysql update row with matched pid
    $result = mysql_query($query);

    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Product successfully updated.";
        
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to update row
		$response["success"] = 0;
		$response["message"] = "Oops! An error occured.";
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
