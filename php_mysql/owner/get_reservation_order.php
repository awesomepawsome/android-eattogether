<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

if (isset($_GET["idReservation"])) {
	$idReservation = $_GET['idReservation'];
    
    $query = "select ao.food_idFood, f.foodName, ao.foodQuantity as quantity
				from attendeeOrder ao
				inner join reservation r
				on ao.reservation_idReservation = r.idReservation
				inner join food f
				on ao.food_idFood = f.idFood
				where r.idReservation = '$idReservation'
				and f.idFood != 1
				group by food_idFood";

	$result = mysql_query($query) or die(mysql_error());

	// check for empty result
	if (mysql_num_rows($result) > 0) {
    	// looping through all results
    	$response["results"] = array();
    
    	while ($row = mysql_fetch_array($result)) {

            $orders = array();
            $orders["idFood"] = $row["food_idFood"];
			$orders["foodName"] = $row["foodName"];
            $orders["quantity"] = $row["quantity"];

        	array_push($response["results"], $orders);
    	}
    
    	// success
    	$response["success"] = 1;
    	// echoing JSON response
    	echo json_encode(utf8ize($response));
    
	} else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
	}

}else{
	// required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);

}

function utf8ize($data) {
    if (is_array($data)) {
        foreach ($data as $k => $v) {
            $data[$k] = utf8ize($v);
        }
    } else if (is_string ($data)) {
        return utf8_encode($data);
    }
    return $data;
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No result found";
    
    return $msgArray;
}

?>