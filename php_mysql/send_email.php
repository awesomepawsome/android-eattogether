<?php

// check for required fields
$isReserveDateTimeSet = isset($_POST['reserveDateTime']) && !empty($_POST['reserveDateTime']);
$isRestaurantNameSet = isset($_POST['restaurantName']) && !empty($_POST['restaurantName']);
$isOrganizerEmailSet = isset($_POST['organizer_email']) && !empty($_POST['organizer_email']);
$isNumOfPaxSet = isset($_POST['numberOfPax']) && !empty($_POST['numberOfPax']);

if ($isReserveDateTimeSet && $isRestaurantNameSet && $isOrganizerEmailSet && $isNumOfPaxSet) {
	
	$reserveDateTime = $_POST['reserveDateTime'];
    $restaurantName = $_POST['restaurantName'];
    $organizer_email = $_POST['organizer_email'];
    $numberOfPax = $_POST['numberOfPax'];
    
    // the message
	$message = "Restaurant: '$restaurantName'\r\n
	Date/Time: '$reserveDateTime'\r\n
	Organizer email: '$organizer_email'\r\n
	Number of pax: '$numberOfPax'\r\n";

	$to = "$organizer_email";
	$subject = "Reservation confirmation";

	// use wordwrap() if lines are longer than 70 characters
	$msg = wordwrap($message,70);

	$headers = "From: webmaster@eat-together.com";

	mail($to,$subject,$msg,$headers);
	
	$response["success"] = 1;
    $response["message"] = "Email sent successfully";

    echo json_encode($response);

} else {

    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    echo json_encode($response);
}
?>