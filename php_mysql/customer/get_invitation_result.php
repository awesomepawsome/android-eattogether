<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

if (isset($_GET["idTempReserve"])) {
	$idTempReserve = $_GET['idTempReserve'];
    
    $query = "select ic.attendance, count(ic.attendance) as occurrence
				from invitingCandidate ic
				where ic.tempReserve_idTempReserve = '$idTempReserve'
				group by ic.attendance
				order by occurrence desc";

	$result = mysql_query($query) or die(mysql_error());

	// check for empty result
	if (mysql_num_rows($result) > 0) {
    	// looping through all results
    	$response["result"] = array();
    
    	while ($row = mysql_fetch_array($result)) {
        	$invitationResult = array();
            $invitationResult["attendance"] = $row["attendance"];
            $invitationResult["occurrence"] = $row["occurrence"];

        	array_push($response["result"], $invitationResult);
    	}
    
    	// success
    	$response["success"] = 1;
    	// echoing JSON response
    	echo json_encode(utf8ize($response));
    
	} else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
	}

}else{
	// required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);

}

function utf8ize($data) {
    if (is_array($data)) {
        foreach ($data as $k => $v) {
            $data[$k] = utf8ize($v);
        }
    } else if (is_string ($data)) {
        return utf8_encode($data);
    }
    return $data;
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No result found";
    
    return $msgArray;
}

?>
