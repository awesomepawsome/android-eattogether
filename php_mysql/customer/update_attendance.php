<?php

// array for JSON response
$response = array();

// check for required fields
$isAllFieldsSet = isset($_POST['isAttend']) && isset($_POST['idTempReserve']) && isset($_POST['email']);
if ($isAllFieldsSet) {
    
    $isAttend = $_POST['isAttend'];
    $idTempReserve = $_POST['idTempReserve'];
    $email = $_POST['email'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    
    $query = "update invitingCandidate
				set attendance = '$isAttend'
				where tempreserve_idtempreserve = '$idTempReserve'
				and customer_invited = '$email'";

    // mysql update row with matched pid
    $result = mysql_query($query);

    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Successfully updated.";
        
        // echoing JSON response
        echo json_encode($response);
    } else {
        
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
