<?php

// array for JSON response
$response = array();

// check for required fields
$isAllFieldsSet = isset($_POST['idFood']) && isset($_POST['foodQuantity']) && isset($_POST['email']) && isset($_POST['idReservation']);
if ($isAllFieldsSet) {

    $idFood = $_POST['idFood'];    
    $foodQuantity = $_POST['foodQuantity'];
    $email = $_POST['email'];
    $idReservation = $_POST['idReservation'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    
    // invited participants able to update food order anytime before the event once confirm the participation
    $query = "UPDATE attendeeorder
				SET food_idFood = '$idFood', foodQuantity = '$foodQuantity'
				WHERE customer_attendee = '$email'
				AND reservation_idReservation = '$idReservation' ";

    // mysql update row with matched pid
    $result = mysql_query($query);

    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Successfully updated.";
        
        // echoing JSON response
        echo json_encode($response);
    } else {
        
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
