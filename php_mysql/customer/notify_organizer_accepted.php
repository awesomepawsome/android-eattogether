<?php 

$isAllFieldSet = isset($_GET['responder_email']) && isset($_GET['idTempReserve']) && isset($_GET['status']);

function send_notification ($tokens, $message)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $tokens,
			 'data' => $message
			);
		$headers = array(
			'Authorization:key = AIzaSyDMd7rTImrowe9nBRVstP_3hAkwCqG2KEI',
			'Content-Type: application/json'
			);
	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
	}

if ($isAllFieldSet) {


	$responderEmail = $_GET['responder_email'];
	$idTempReserve = $_GET['idTempReserve'];
	$respond = $_GET['status'];
	
	$conn = mysqli_connect("localhost","root","","eat_together");
	$sql = "Select c.firebaseId
			from TempReserve t
			inner join customer c
			on t.Customer_organizer = c.Login_email
			where idTempReserve = $idTempReserve ";
	$result = mysqli_query($conn,$sql);
	$tokens = array();
	if(mysqli_num_rows($result) > 0 ){
		while ($row = mysqli_fetch_assoc($result)) {
				$tokens[] = $row["firebaseId"];
			}
	}
	mysqli_close($conn);

	if($respond == '0'){
		$message = array("message" => $responderEmail .' has rejected your invitation.');
		$message_status = send_notification($tokens, $message);
		echo $message_status;
	}else{
		$message = array("message" => $responderEmail .' has accepted your invitation.');
		$message_status = send_notification($tokens, $message);
		echo $message_status;
	}
	
}
 ?>