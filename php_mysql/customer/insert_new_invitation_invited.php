<?php

// array for JSON response
$response = array();

// check for required fields
$isInvitedEmailSet = isset($_POST['invited_email']) && !empty($_POST['invited_email']);

if ($isInvitedEmailSet) {
    
    $invited_email = $_POST['invited_email'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    	
	// get last insert auto increment key and put into idTempRestaurant
	$queryMaxIdTempReserve = "select max(idTempReserve) as idTempReserve from TempReserve";
	$result = mysql_query($queryMaxIdTempReserve);
	
    if (mysql_num_rows($result) == 1) {
	    $value = mysql_fetch_array($result);            
        $idTempRestaurant = $value["idTempReserve"];
        
		$queryInvitingCandidate = "INSERT INTO InvitingCandidate (TempReserve_idTempReserve, Customer_invited)
			VALUES ('$idTempRestaurant', '$invited_email')";
	    $resulCustomer = mysql_query($queryInvitingCandidate);
    }
    
    // check if row inserted or not
    if ($resulCustomer) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Invitation successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! The invitation creation failed.";
        
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>