<?php

// array for JSON response
$response = array();

// check for required fields
$isTempDateTimeSet = isset($_POST['tempDateTime']) && !empty($_POST['tempDateTime']);
$isIdRestaurantSet = isset($_POST['idRestaurant']) && !empty($_POST['idRestaurant']);
$isOrganizerEmailSet = isset($_POST['organizer_email']) && !empty($_POST['organizer_email']);

if ($isTempDateTimeSet && $isIdRestaurantSet && $isOrganizerEmailSet) {
    
    $tempDateTime = $_POST['tempDateTime'];
    $idRestaurant = $_POST['idRestaurant'];
    $organizer_email = $_POST['organizer_email'];
//     $invited_email = $_POST['invited_email'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    
    // by default validity is set to true, means still under temporary reservation
	// dateTime format: YYYY-MM-DD HH:mm:ss (e.g. 2016-07-13 00:00:00)

    $queryTempReserve = "INSERT INTO tempReserve (tempDateTime, idRestaurant, validity, customer_organizer)
		VALUES ('$tempDateTime', '$idRestaurant', true, '$organizer_email')";
	$resulTempReserve = mysql_query($queryTempReserve);	
	
    // check if row inserted or not
    if ($resulTempReserve) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Invitation successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! The invitation creation failed.";
        
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>