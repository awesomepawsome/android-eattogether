<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

// check for post data
if (isset($_GET["idVoteRestaurant"])) {
    $idVoteRestaurant = $_GET['idVoteRestaurant'];
    
    // query highest voting result, if return nil means no highest vote obtain yet
	// send notification if result is not nil
    $query = "select case when occurrence > (invited / 2) then voting.restaurantSelection end as 'restaurantSelection', occurrence
				from
				(select vc.restaurantSelection, count(vc.restaurantSelection) as occurrence
					from votingcandidate vc
					where vc.voterestaurant_idVoteRestaurant = '$idVoteRestaurant'
					group by vc.restaurantSelection
					order by occurrence desc
					limit 1) voting,
				(select count(*) as invited
					from votingcandidate vc
					where vc.voterestaurant_idVoteRestaurant = '$idVoteRestaurant') invitation";
				
    $result = mysql_query($query);

    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {

            $result = mysql_fetch_array($result);

            $votingResult = array();
            $votingResult["occurrence"] = $result["occurrence"];
            $votingResult["restaurantSelection"] = $result["restaurantSelection"];
            // success
            $response["success"] = 1;

            // user node
            $response["result"] = array();

            array_push($response["result"], $votingResult);

            // echoing JSON response
            echo json_encode($response);
        } else {
            $response = resultNotFoundMsg();
            echo json_encode($response);
        }
    } else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No result found";
    
    return $msgArray;
}

?>