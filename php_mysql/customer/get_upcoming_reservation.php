<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

if (isset($_GET["email"])) {
	$email = $_GET['email'];
    
    $query = "select distinct(idReservation) AS idReservation, reserveDateTime as reserveDateTime, numberOfPax, idRestaurant, restaurantName, photo, type, address, false AS validity 
    from reservation, AttendeeOrder, Restaurant 
    where (Customer_organizer = '$email' OR Customer_attendee = '$email') 
    AND Restaurant_idRestaurant = idRestaurant 
    AND idReservation = Reservation_idReservation 
    AND DATE(reserveDateTime) >= CURDATE() 
UNION 
	select idVoteRestaurant AS idReservation, voteDateTime as reserveDateTime, 0 AS numberOfPax, 0 as idRestaurant, appendIdRestaurant AS restaurantName, null as photo, '' AS type, '' AS address, validity 
	FROM VoteRestaurant, Restaurant 
	WHERE Customer_organizer = '$email'
	AND validity = 1  
	AND DATE(voteDateTime) >= CURDATE() 
UNION 
	select idTempReserve as idReservation, tempDateTime as reserveDateTime, (SELECT COUNT(Customer_invited) FROM InvitingCandidate WHERE idReservation = TempReserve_idTempReserve AND attendance = true) AS numberOfPax, Restaurant.idRestaurant AS idRestaurant, restaurantName, photo, type, address, validity 
	FROM TempReserve, Restaurant 
	WHERE Customer_organizer = '$email' 
	AND TempReserve.idRestaurant = Restaurant.idRestaurant 
	AND validity = 1 
	AND DATE(tempDateTime) >= CURDATE() 
	ORDER BY reserveDateTime";
    
	$result = mysql_query($query) or die(mysql_error());

	// check for empty result
	if (mysql_num_rows($result) > 0) {
    	// looping through all results
    	$response["result"] = array();
    
    	while ($row = mysql_fetch_array($result)) {
        	$upcomingReservation = array();
            $upcomingReservation["idReservation"] = $row["idReservation"];
            $upcomingReservation["reserveDateTime"] = $row["reserveDateTime"];
            $upcomingReservation["numberOfPax"] = $row["numberOfPax"];
            $upcomingReservation["idRestaurant"] = $row["idRestaurant"];
            $upcomingReservation["restaurantName"] = $row["restaurantName"];
            $upcomingReservation["photo"] = $row["photo"];
            $upcomingReservation["type"] = $row["type"];
            $upcomingReservation["address"] = $row["address"];
            $upcomingReservation["validity"] = $row["validity"];

        	array_push($response["result"], $upcomingReservation);
    	}
    
    	// success
    	$response["success"] = 1;
    	// echoing JSON response
    	echo json_encode(utf8ize($response));
    
	} else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
	}

}else{
	// required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);

}

function utf8ize($data) {
    if (is_array($data)) {
        foreach ($data as $k => $v) {
            $data[$k] = utf8ize($v);
        }
    } else if (is_string ($data)) {
        return utf8_encode($data);
    }
    return $data;
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No result found";
    
    return $msgArray;
}

?>