<?php

// array for JSON response
$response = array();

// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

// check for post data
if (isset($_GET["idRestaurant"]) && isset($_GET["reserveDateTime"])) {
	$idRestaurant = $_GET['idRestaurant'];
    $reserveDateTime = $_GET['reserveDateTime'];
    
    $query = "select count(DATE_FORMAT(v.reserveDateTime, '%Y-%m-%d %H:%m:%s')) as bookedTable, r.nTable
				from restaurant r
				inner join reservation v
				on r.idRestaurant = v.Restaurant_idRestaurant
				where r.idRestaurant = '$idRestaurant'
				AND DATE_FORMAT(v.reserveDateTime, '%Y-%m-%d %H:%m:%s')
				BETWEEN DATE_SUB(DATE_FORMAT('$reserveDateTime', '%Y-%m-%d %H:%m:%s'), INTERVAL 30 MINUTE)
				and DATE_ADD(DATE_FORMAT('$reserveDateTime', '%Y-%m-%d %H:%m:%s'), INTERVAL 30 MINUTE)
				order by v.reserveDateTime";

    $result = mysql_query($query);

    if (!empty($result)) {
        // check for empty result
        if (mysql_num_rows($result) > 0) {

            $result = mysql_fetch_array($result);
            
            if ($result["bookedTable"] < $result["nTable"]) {
	            // success
            	$response["success"] = 1;
            	$response["message"] = "Available";
            } else {
				// failed
            	$response["success"] = 0;
            	$response["message"] = "Sorry table is fully booked for this slot";
            }            

            // echoing JSON response
            echo json_encode($response);
        } else {
            $response = resultNotFoundMsg();
            echo json_encode($response);
        }
    } else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No result found";
    
    return $msgArray;
}

?>