<?php 

$organizerEmailSet = isset($_POST['organizer_email']) && !empty($_POST['organizer_email']);

function send_notification ($tokens, $message)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $tokens,
			 'data' => $message
			);
		$headers = array(
			'Authorization:key = AIzaSyDMd7rTImrowe9nBRVstP_3hAkwCqG2KEI',
			'Content-Type: application/json'
			);
	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
	}

if ($organizerEmailSet) {
	$organizerEmail = $_POST['organizer_email'];
	
	$conn = mysqli_connect("localhost","root","","eat_together");
	$sql = "Select idTempReserve, Customer_organizer From tempreserve
		where Customer_organizer = '$organizerEmail'
		and idTempReserve = (select max(idTempReserve) from tempreserve)";
	$resultBooking = mysqli_query($conn,$sql);
	
	if(mysqli_num_rows($resultBooking) > 0 ){
		$row = mysqli_fetch_assoc($resultBooking);
		$idTempReserve = $row["idTempReserve"];
		$query = "Select c.firebaseId
			from invitingcandidate i
			inner join customer c
			on i.Customer_invited = c.Login_email
			where TempReserve_idTempReserve = '$idTempReserve'";
		$resultInvite = mysqli_query($conn,$query);
		$tokens = array();
		if(mysqli_num_rows($resultInvite) > 0 ){
			while ($row = mysqli_fetch_assoc($resultInvite)) {
				$tokens[] = $row["firebaseId"];
			}
		}
	}
	mysqli_close($conn);
	$message = array("message" => "You have an invitation!");
	$message_status = send_notification($tokens, $message);
	echo $message_status;
	
}
	
 ?>