<?php

// array for JSON response
$response = array();

// check for required fields
$isReserveDateTimeSet = isset($_POST['reserveDateTime']) && !empty($_POST['reserveDateTime']);
$isIdRestaurantSet = isset($_POST['idRestaurant']) && !empty($_POST['idRestaurant']);
$isOrganizerEmailSet = isset($_POST['organizer_email']) && !empty($_POST['organizer_email']);
$isNumOfPaxSet = isset($_POST['numberOfPax']) && !empty($_POST['numberOfPax']);
$isIdTempReserveSet = isset($_POST['idReservation']) && !empty($_POST['idReservation']);
$isVoteSet = isset($_POST['newVote']) && !empty($_POST['newVote']);

function send_notification ($tokens, $message)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $tokens,
			 'data' => $message
			);
		$headers = array(
			'Authorization:key = AIzaSyDMd7rTImrowe9nBRVstP_3hAkwCqG2KEI',
			'Content-Type: application/json'
			);
	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
	}


if ($isReserveDateTimeSet && $isIdRestaurantSet && $isOrganizerEmailSet && $isNumOfPaxSet) {
    
    $reserveDateTime = $_POST['reserveDateTime'];
    $idRestaurant = $_POST['idRestaurant'];
    $organizer_email = $_POST['organizer_email'];
    $numberOfPax = $_POST['numberOfPax'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    
    $queryIdMax = "Select MAX(idReservation)+1 AS idReservation FROM Reservation";
    $resultId = mysql_query($queryIdMax) or die(mysql_error());
    $idReservation = 0;
    if (mysql_num_rows($resultId) > 0) {
    	// looping through all results
    	$response["result"] = array();
    
    	while ($row = mysql_fetch_array($resultId)) {
        	$idReservation = $row["idReservation"];
    	}
    }
    
    // by default validity is set to true, means still under temporary reservation
	// dateTime format: YYYY-MM-DD HH:mm:ss (e.g. 2016-07-13 00:00:00)
	$queryReserve = "insert into Reservation (idReservation, reserveDateTime, numberOfPax, Customer_organizer, Restaurant_idRestaurant)
		values ('$idReservation' ,'$reserveDateTime', '$numberOfPax', '$organizer_email', '$idRestaurant')";
	$resulReserve = mysql_query($queryReserve);
		
    // check if row inserted or not
    if ($resulReserve) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Reservation successfully created.";

        // echoing JSON responseff
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! The reservation creation failed.";
        
        // echoing JSON response
        echo json_encode($response);
    }
    
    //if more than one pax, add to AttendeeOrder and remove TempReserve
    if(($numberOfPax > 1) && $isIdTempReserveSet && $isVoteSet){
    	$idTempReserve = $_POST['idReservation'];
    	$vote = $_POST['newVote'];
    	
    	if($vote == 0){
    		//TempReservation
    		$query = "select Customer_invited, firebaseId
				FROM InvitingCandidate   
				INNER JOIN Customer 
				ON Customer_invited = Login_email 
				WHERE TempReserve_idTempReserve = '$idTempReserve'  
				AND attendance = true";
				
			$invalidate_temp = "update TempReserve set validity = false WHERE idTempReserve = '$idTempReserve'";
    	}else{
    		//Invalidate voting
    		$query = "select Customer_invited, firebaseId
				FROM VotingCandidate   
				INNER JOIN Customer
				ON Customer_invited = Login_email
				WHERE VoteRestaurant_idVoteRestaurant = '$idTempReserve'  
				AND restaurantSelection = '$idRestaurant'";
				
			$invalidate_temp = "UPDATE VoteRestaurant SET validity = false WHERE idVoteRestaurant = '$idTempReserve'";
    	}
		$result = mysql_query($query) or die(mysql_error());

		$tokens = array();		
		// check for empty result
		if (mysql_num_rows($result) > 0) {
    		// looping through all results
    		$response["result"] = array();
    
    		while ($row = mysql_fetch_array($result)) {
        		$attendeeEmail = $row["Customer_invited"];
        		$tokens[] = $row["firebaseId"];
        		
				$queryAttendeeOrder = "insert into AttendeeOrder (Customer_attendee, Reservation_idReservation, Food_idFood)
					values ('$attendeeEmail', '$idReservation', '1')";        

				$resulAttendeeOrder = mysql_query($queryAttendeeOrder);	
				if ($resulAttendeeOrder) {
        			// successfully inserted into database
        			$response["success"] = 1;
        			$response["message"] = "AttendeeOrder successfully added.";

        			// echoing JSON responseff
        			echo json_encode($response);
    			}else{
    				// failed to delete row
        			$response["success"] = 0;
        			$response["message"] = "Oops! Failed to add AttendeeOrder.";
        
        			// echoing JSON response
        			echo json_encode($response);
    			}
    			
    		}
    		
    		//send notifications
    		$message = array("message" => $organizer_email.' has confirmed an event.');
			$message_status = send_notification($tokens, $message);
    		
    	}
    	
    	$resulDeleteTemp = mysql_query($invalidate_temp);

    	// check if row deleted or not
	    if ($resulDeleteTemp) {
    	    // successfully inserted into database
        	$response["success"] = 1;
	        $response["message"] = "Disable tempory reservation/voting is success.";

        	// echoing JSON responseff
    	    echo json_encode($response);
	    } else {
        	// failed to delete row
    	    $response["success"] = 0;
	        $response["message"] = "Unable to set TempReserve invalid";
        
        	// echoing JSON response
        	echo json_encode($response);
    	}
	}
	
} else {

    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    echo json_encode($response);
}
?>