<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

// check for post data
if (isset($_GET["Login_email"])) {
    $Login_email = $_GET['Login_email'];
    
    $query_invite = "select t.idTempReserve, r.idRestaurant, r.restaurantName, t.tempDateTime, t.Customer_organizer
				from tempreserve t
				inner join restaurant r
				on t.idRestaurant = r.idRestaurant
				inner join invitingcandidate i
				on t.idTempReserve = i.TempReserve_idTempReserve
				where i.Customer_invited = '$Login_email'
				and t.validity is true
                and i.attendance is null
                and DATE(t.tempDateTime) >= CURDATE()
				order by t.idTempReserve desc";
    $result_invite = mysql_query($query_invite);
				
	$query_vote = "select vr.idVoteRestaurant, vr.voteDateTime, vr.appendIdRestaurant, vr.validity, vr.Customer_organizer
					from votingCandidate vc
					inner join voteRestaurant vr
					on vc.voteRestaurant_idVoteRestaurant = vr.idVoteRestaurant
					where vc.customer_invited = '$Login_email'
					and DATE(vr.voteDateTime) >= CURDATE()
					and vc.restaurantSelection is null
					and vr.validity is true";
    $result_vote = mysql_query($query_vote);
    
    $query_invite_response = "SELECT idTempReserve, restaurantName,  Customer_invited, attendance, tempDateTime 
    							FROM TempReserve, InvitingCandidate, Restaurant
    							WHERE Customer_organizer = '$Login_email' 
    							AND DATE(tempDateTime) >= CURDATE() 
    							AND idTempReserve = TempReserve_idTempReserve 
    							AND validity = 1 
    							AND (attendance = 0 OR attendance = 1) 
    							AND Restaurant.idRestaurant = TempReserve.idRestaurant"; 
    $result_response = mysql_query($query_invite_response);

    if (!empty($result_invite) || !empty($result_vote) || !empty($result_response)) {
	      // user node
      	$response["invitation"] = array();
    
        if (mysql_num_rows($result_invite) > 0) {
	        while ($row = mysql_fetch_array($result_invite)) {
					$invitation = array();
					$invitation["idTempReserve"] = $row["idTempReserve"];
					$invitation["restaurantName"] = $row["restaurantName"];
					$invitation["tempDateTime"] = $row["tempDateTime"];
					$invitation["Customer_organizer"] = $row["Customer_organizer"];
					$invitation["type"] = "i";
	
					array_push($response["invitation"], $invitation);
				}
        
        }
							
		if (mysql_num_rows($result_vote) > 0) {
			while ($row = mysql_fetch_array($result_vote)) {
				$vote = array();
				$vote["idTempReserve"] = $row["idVoteRestaurant"];
				$vote["restaurantName"] = $row["appendIdRestaurant"];
				$vote["tempDateTime"] = $row["voteDateTime"];
				$vote["Customer_organizer"] = $row["Customer_organizer"];
				$vote["type"] = "v";

				array_push($response["invitation"], $vote);
			}  		
		}       
		
		if (mysql_num_rows($result_response) > 0) {
			while ($row = mysql_fetch_array($result_response)) {
				$invitation = array();
				$invitation["idTempReserve"] = $row["idTempReserve"];
				$invitation["restaurantName"] = $row["restaurantName"];
				$invitation["tempDateTime"] = $row["attendance"];
				$invitation["Customer_organizer"] = $row["Customer_invited"];
				$invitation["type"] = "r";

				array_push($response["invitation"], $invitation);
			}  		
		}   
            
        // success
		$response["success"] = 1;

        // echoing JSON response
		echo json_encode($response);
    } else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No user found";
    
    return $msgArray;
}

?>