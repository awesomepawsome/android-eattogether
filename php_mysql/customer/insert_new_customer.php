<?php

// array for JSON response
$response = array();

// check for required fields
$isEmailSet = isset($_POST['email']) && !empty($_POST['email']);
$isPasswordSet = isset($_POST['password']) && !empty($_POST['password']);
$isDisplayNameSet = isset($_POST['displayName']) && !empty($_POST['displayName']);
$isPhoneSet = isset($_POST['phone']) && !empty($_POST['phone']);
$isFirebaseSet = isset($_POST['firebaseId']) && !empty($_POST['firebaseId']);

if ($isEmailSet && $isPasswordSet && $isDisplayNameSet && $isPhoneSet && $isFirebaseSet) {
    
    $email = $_POST['email'];
    $password = $_POST['password'];
    $displayName = $_POST['displayName'];
    $phone = $_POST['phone'];
    $firebaseId = $_POST['firebaseId'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    	    
    $queryLogin = "insert into login (email, password)
		select * from (select '$email', '$password') as temp
		where not exists(
			select email from login where email = '$email' 
		)";

    $queryCustomer = "insert into customer (displayName, phone, firebaseId, login_email)
		select * from (select '$displayName', '$phone' , '$firebaseId', '$email') as temp
		where exists(
			select email from login where email = '$email' 	
		)";

    // mysql inserting a new row
    $resultLogin = mysql_query($queryLogin);
    $resultCustomer = mysql_query($queryCustomer);

    // check if row inserted or not
    if ($resultLogin && $resultCustomer) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "User successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! The email is a registered account with us.";
        
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>