<?php

// array for JSON response
$response = array();

// check for required fields
$isEmailSet = isset($_POST['email']) && !empty($_POST['email']);
$isIdFoodSet = isset($_POST['idFood']) && !empty($_POST['idFood']);
$isQuantitySet = isset($_POST['quantity']) && !empty($_POST['quantity']);

if ($isEmailSet) {
    
    $email = $_POST['email'];
    $idFood = $_POST['idFood'];
    $quantity = $_POST['quantity'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    
	// get last insert auto increment key and put into idReservation
	$queryMaxIdReservation = "select max(idReservation) as idReservation from Reservation";
	$result = mysql_query($queryMaxIdReservation);
	
	if (mysql_num_rows($result) == 1) {
	    $value = mysql_fetch_array($result);            
        $idReservation = $value["idReservation"];
        
        if ($isIdFoodSet && $isQuantitySet) {
        	$queryAttendeeOrder = "insert into AttendeeOrder (Customer_attendee, Reservation_idReservation, Food_idFood, foodQuantity)
				values ('$email', '$idReservation', '$idFood', '$quantity')";
        $resulAttendeeOrder = mysql_query($queryAttendeeOrder);	
        } 
    }
		
    // check if row inserted or not
    if ($resulAttendeeOrder) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Food successfully created.";

        // echoing JSON responseff
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! The food creation failed.";
        
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>