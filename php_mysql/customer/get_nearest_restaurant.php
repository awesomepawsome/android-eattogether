<?php

// array for JSON response
$response = array();


// include db connect class
require_once __DIR__ . '/../db_connect.php';

// connecting to db
$db = new Db_Connect();

if (isset($_GET["fLatitude"]) && isset($_GET["fLongitude"])) {
	$fLatitude = $_GET['fLatitude'];
    $fLongitude = $_GET['fLongitude'];
    
    $query = "SELECT idRestaurant, restaurantName, type, website, phone, address, operatingHour, photo, truncate(
				ACOS( SIN( RADIANS( `latitude` ) ) * SIN( RADIANS( $fLatitude ) ) + COS( RADIANS( `latitude` ) )
				* COS( RADIANS( $fLatitude )) * COS( RADIANS( `longitude` ) - RADIANS( $fLongitude )) ) * 6380, 1) AS distance
				FROM restaurant
				WHERE
				ACOS( SIN( RADIANS( `latitude` ) ) * SIN( RADIANS( $fLatitude ) ) + COS( RADIANS( `latitude` ) )
				* COS( RADIANS( $fLatitude )) * COS( RADIANS( `longitude` ) - RADIANS( $fLongitude )) ) * 6380 < 20
				ORDER BY distance";

	$result = mysql_query($query) or die(mysql_error());

	// check for empty result
	if (mysql_num_rows($result) > 0) {
    	// looping through all results
    	$response["results"] = array();
    
    	while ($row = mysql_fetch_array($result)) {

        	$restaurants = array();
        	$restaurants["idRestaurant"] = $row["idRestaurant"];
        	$restaurants["restaurantName"] = $row["restaurantName"];
        	$restaurants["type"] = $row["type"];
        	$restaurants["website"] = $row["website"];
    		$restaurants["phone"] = $row["phone"];
        	$restaurants["address"] = $row["address"];
        	$restaurants["operatingHour"] = $row["operatingHour"];
			$restaurants["photo"] = $row["photo"];
        	$restaurants["distance"] = $row["distance"];

        	array_push($response["results"], $restaurants);
    	}
    
    	// success
    	$response["success"] = 1;
    	// echoing JSON response
    	echo json_encode(utf8ize($response));
    
	} else {
    	$response = resultNotFoundMsg();
        echo json_encode($response);
	}

}else{
	// required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);

}

function utf8ize($data) {
    if (is_array($data)) {
        foreach ($data as $k => $v) {
            $data[$k] = utf8ize($v);
        }
    } else if (is_string ($data)) {
        return utf8_encode($data);
    }
    return $data;
}

function resultNotFoundMsg() {
	$msgArray = array();
	
	// no product found
    $msgArray["success"] = 0;
    $msgArray["message"] = "No result found";
    
    return $msgArray;
}

?>
