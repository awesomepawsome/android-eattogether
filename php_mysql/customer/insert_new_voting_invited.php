<?php

// array for JSON response
$response = array();

// check for required fields
$isInvitedEmailSet = isset($_POST['invited_email']) && !empty($_POST['invited_email']);

function send_notification ($tokens, $message)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $tokens,
			 'data' => $message
			);
		$headers = array(
			'Authorization:key = AIzaSyDMd7rTImrowe9nBRVstP_3hAkwCqG2KEI',
			'Content-Type: application/json'
			);
	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
	}
	
if ($isInvitedEmailSet) {
    
    $invited_email = $_POST['invited_email'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    	
	// get last insert auto increment key and put into idTempRestaurant
	$queryMaxIdVote = "select max(idVoteRestaurant) as idVoteRestaurant from VoteRestaurant";
	$result = mysql_query($queryMaxIdVote);
	
    if (mysql_num_rows($result) == 1) {
	    $value = mysql_fetch_array($result);            
        $idVoteRestaurant = $value["idVoteRestaurant"];
        
		$queryInvitingCandidate = "INSERT INTO votingcandidate (voteRestaurant_idVoteRestaurant, customer_invited)
			VALUES ('$idVoteRestaurant', '$invited_email')";
	    $resulCustomer = mysql_query($queryInvitingCandidate);
    }
    
    // check if row inserted or not
    if ($resulCustomer) {
        // successfully inserted into database
        $response["success"] = 1;
        $response["message"] = "Invitation successfully created.";

        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["success"] = 0;
        $response["message"] = "Oops! The invitation creation failed.";
        
        // echoing JSON response
        echo json_encode($response);
    }
    
    //send push notification
    $query = "SELECT firebaseId 
    FROM Customer 
    WHERE Login_email = '$invited_email'";
    $resultNotification = mysql_query($query);
    $tokens = array();
    if(mysql_num_rows($resultNotification) > 0){
    	while($row = mysql_fetch_array($resultNotification)){
    		$tokens[] = $row["firebaseId"];
    	}
    }
    
    $message = array("message" => "You received a voting request.");
    $message_status = send_notification($tokens,$message);
    
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>