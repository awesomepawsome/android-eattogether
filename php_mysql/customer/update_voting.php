<?php

// array for JSON response
$response = array();

// check for required fields
$isAllFieldsSet = isset($_POST['idRestaurant']) && isset($_POST['idVoteRestaurant']) && isset($_POST['email']);

function send_notification ($tokens, $message)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $tokens,
			 'data' => $message
			);
		$headers = array(
			'Authorization:key = AIzaSyDMd7rTImrowe9nBRVstP_3hAkwCqG2KEI',
			'Content-Type: application/json'
			);
	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
	}
	

if ($isAllFieldsSet) {
    
    $idRestaurant = $_POST['idRestaurant'];
    $idVoteRestaurant = $_POST['idVoteRestaurant'];
    $email = $_POST['email'];
	echo 'ID '.$idVoteRestaurant;
    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();

    $query = "UPDATE votingcandidate
				SET restaurantSelection = '$idRestaurant'
				WHERE voterestaurant_idVoteRestaurant = '$idVoteRestaurant'
				AND customer_invited = '$email' ";

    // mysql update row with matched pid
    $result = mysql_query($query);

    // check if row inserted or not
    if ($result) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Successfully updated.";
        
        // echoing JSON response
        echo json_encode($response);
    } else {
        
    }

        
    $notificationQuery = "SELECT c.firebaseId 
    	FROM VoteRestaurant v, Customer c
    	WHERE v.idVoteRestaurant = '$idVoteRestaurant' 
    	AND v.Customer_organizer = c.Login_email";
    $res = mysql_query($notificationQuery);
    $tokens = array();
    if(mysql_num_rows($res) > 0){
    	while($rowRes = mysql_fetch_array($res)){
    		$tokens[] = $rowRes["firebaseId"];
    	}
    }
    
    $message = array("message" => $email.' has responded to your voting request.');
    $message_status = send_notification($tokens,$message);
    
    
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
