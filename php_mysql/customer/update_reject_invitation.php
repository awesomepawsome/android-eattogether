<?php

// array for JSON response
$response = array();

// check for required fields
$isIdTempReserveSet = isset($_POST['idTempReserve']) && !empty($_POST['idTempReserve']);

function send_notification ($tokens, $message) {
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			 'registration_ids' => $tokens,
			 'data' => $message
			);
		$headers = array(
			'Authorization:key = AIzaSyDMd7rTImrowe9nBRVstP_3hAkwCqG2KEI',
			'Content-Type: application/json'
			);
	   $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_POST, true);
       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
       $result = curl_exec($ch);           
       if ($result === FALSE) {
           die('Curl failed: ' . curl_error($ch));
       }
       curl_close($ch);
       return $result;
}
	
if ($isIdTempReserveSet) {
    
    $idTempReserve = $_POST['idTempReserve'];

    // include db connect class
    require_once __DIR__ . '/../db_connect.php';

    // connecting to db
    $db = new Db_Connect();
    		
    $queryOrganizer = "update TempReserve
				set validity = false
				where idTempReserve = '$idTempReserve'";
				
	$resultOrganizer = mysql_query($queryOrganizer);
		
    
    //send push notification
    $query = "SELECT c.firebaseId 
    FROM Customer c, InvitingCandidate i
    WHERE c.Login_email = i.Customer_invited
    AND i.TempReserve_idTempReserve = '$idTempReserve'";
    $resultNotification = mysql_query($query);
    $tokens = array();
    
    if($resultOrganizer && mysql_num_rows($resultNotification) > 0){
    	while($row = mysql_fetch_array($resultNotification)){
    		$tokens[] = $row["firebaseId"];
    	}
    }
    
    $message = array("message" => "You received a voting request.");
    $message_status = send_notification($tokens,$message);
    
    $response["success"] = 1;
    $response["message"] = "Marked invitation as invalid";
    
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>