-- new user registration with checking no duplicate registration
-- solution: http://stackoverflow.com/questions/12679241/php-mysql-begin-commit-not-working
-- begin;
	insert into login (email, password)
	select * from (select '$email', '$password') as temp
	where not exists(
		select email from login where email = '$email' 
	);
	insert into customer (displayName, phone, login_email)
	select * from (select '$displayName', '$phone' , '$email') as temp
	where exists(
		select email from login where email = '$email' 	
	);
-- commit;

-- *********** --


-- *********** --

-- user invites friend for meal

-- by default validity is set to true, means still under temporary reservation
-- dateTime format: YYYY-MM-DD HH:mm:ss (e.g. 2016-07-13 00:00:00)

-- begin;
	INSERT INTO tempReserve (tempDateTime, idRestaurant, validity, customer_organizer)
	VALUES ('$tempDateTime', '$idRestaurant', true, '$organizer_email');
	
	-- get last insert auto increment key and put into idTempRestaurant
	$db->insert_id

	INSERT INTO invitingCandidate (tempReserve_idTempReserve, customer_invited)
	VALUES ('$idTempRestaurant', '$invited_email');
-- commit;

-- reservation for one
insert into Reservation (reserveDateTime, numberOfPax, Customer_organizer, Restaurant_idRestaurant)
values ('$reserveDateTime', 1, '$email', '$idRestaurant')

insert into AttendeeOrder (Customer_attendee, Reservation_idReservation, Food_idFood, foodQuantity)
values ('$email', '$idReservation', '$idFood', 'quantity')

-- if user proceed with booking, this temporary invitation is no longer valid
update tempReserve
set validity = false
where idTempReserve = '$idTempReserve'

-- *********** --

-- user invites friend for meal with voting option
-- by default validity is set to true, means the voting still valid
-- begin;
	INSERT INTO voterestaurant (voteDateTime, appendIdRestaurant, validity, customer_organizer)
	VALUES ('$voteDateTime', '$append_idRestaurant', true, '$organizer_email');

	INSERT INTO votingcandidate (voteRestaurant_idVoteRestaurant, customer_invited, restaurantSelection)
	VALUES ('$idVoteRestaurant', '$invited_email', '$restaurantSelection');
	-- $selection is id from Restaurant table but no relationship defination is needed
-- commit;

-- invited user votes for restaurant
UPDATE votingcandidate
SET restaurantSelection = '$idRestaurant'
WHERE voterestaurant_idVoteRestaurant = '$idVoteRestaurant'
AND customer_invited = '$email' ;

-- show current voting result
select vc.restaurantSelection, count(vc.restaurantSelection) as occurrence
from votingcandidate vc
where vc.voterestaurant_idVoteRestaurant = '$idVoteRestaurant'
group by vc.restaurantSelection
order by occurrence desc


-- mark voting as closed if organiser proceed with the voting booking or exceed the time
UPDATE voterestaurant
SET validity = false
WHERE idVoteRestaurant = '$idVoteRestaurant';

-- *********** --

-- user make a reservation
-- begin;
	INSERT INTO reservation (reserveDateTime, numberOfPax, customer_organizer, restaurant_idRestaurant)
	VALUES ('$dateTime', '$numberOfPax', '$organizer_email', '$idRestaurant');
	
	-- food id can be null for invited, but organiser cannot be null
	INSERT INTO attendeeorder (customer_attendee, reservation_idReservation, food_idFood, foodQuantity)
	VALUES ('$attendee_email', '$idReservation', '$idFood', '$quantity');
-- commit;

-- *********** --

-- View table availability
select count(DATE_FORMAT(v.reserveDateTime, '%Y-%m-%d %H:%m:%s')) as bookedTable, r.nTable
from reservation v
inner join restaurant r
on v.Restaurant_idRestaurant = r.idRestaurant
where v.Restaurant_idRestaurant = '$idRestaurant'
AND DATE_FORMAT(v.reserveDateTime, '%Y-%m-%d %H:%m:%s')
BETWEEN DATE_SUB(DATE_FORMAT('$reserveTime', '%Y-%m-%d %H:%m:%s'), INTERVAL 30 MINUTE)
and DATE_ADD(DATE_FORMAT('$reserveTime', '%Y-%m-%d %H:%m:%s'), INTERVAL 30 MINUTE)
order by v.reserveDateTime

-- View my reservation from today onwards
select idReservation, DATE_FORMAT(reserveDateTime, '%Y-%m-%d'), numberOfPax, restaurant_idRestaurant
from reservation
where customer_organizer = '$email'
and DATE(reserveDateTime) >= CURDATE()
order by reserveDateTime

-- View my invited event (going / not going / pending result)
select *
from invitingCandidate ic
inner join tempReserve tr
on ic.tempReserve_idTempReserve = tr.idTempReserve
where ic.customer_invited = '$email'
and DATE(tr.tempDateTime) >= CURDATE()
and tr.validity = true
order by ic.attendance

-- View my open invitation (pending voting)
select vr.idVoteRestaurant, DATE_FORMAT(vr.voteDateTime, '%Y-%m-%d'), vr.appendIdRestaurant, vr.validity
from votingCandidate vc
inner join voteRestaurant vr
on vc.voteRestaurant_idVoteRestaurant = vr.idVoteRestaurant
where vc.customer_invited = '$email'
and DATE(vr.voteDateTime) >= CURDATE()
and vr.validity = true

