package com.gavin.eat2gether.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.StringUtil;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationRespondVote extends Fragment {

    View view;
    TextView invitationTextView;
    Button acceptButton, rejectButton;

    RadioGroup restaurantRadioGroup;
    RadioButton radioButtonOne, radioButtonTwo;

    String email, idTempReserve, restaurantAppended, eventDateTime, organizerEmail;
    String[] idRestaurant = new String[2];
    int count = 0;

    private SharedPreferences sharedPreferences;

    public NotificationRespondVote() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Bundle args = getArguments();
            idTempReserve = args.getString(Constants.TAG_TEMP_RESERVE);
            restaurantAppended = args.getString(Constants.TAG_RESTAURANT_NAME);
            eventDateTime = args.getString(Constants.TAG_TEMP_DATE_TIME);
            organizerEmail = args.getString(Constants.TAG_ORGANIZER_EMAIL);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(Constants.APP_NAME, "restaurantAppended: " + restaurantAppended);
        idRestaurant[0] = String.valueOf(restaurantAppended.charAt(0));
        idRestaurant[1] = String.valueOf(restaurantAppended.charAt(2));

        String[] passing = new String[2];
        passing[0] = Constants.GET_RESTAURANT_INFO;
        passing[1] = idRestaurant[0];

        RestaurantHttp task = new RestaurantHttp();
        task.execute(passing);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification_respond_vote, container, false);

        getActivity().getActionBar().setTitle("Restaurant Voting");
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
        email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL, null);

        invitationTextView = (TextView) view.findViewById(R.id.tv_event_information);

        restaurantRadioGroup = (RadioGroup) view.findViewById(R.id.radio_group_restaurant);
        radioButtonOne = (RadioButton) view.findViewById(R.id.radio_option_one);
        radioButtonOne.setId(Integer.parseInt(idRestaurant[0]));
        radioButtonTwo = (RadioButton) view.findViewById(R.id.radio_option_two);
        radioButtonTwo.setId(Integer.parseInt(idRestaurant[1]));

        acceptButton = (Button) view.findViewById(R.id.button_accept_invitation);
        acceptButton.setOnClickListener(acceptButtonOnClickListener);

        rejectButton = (Button) view.findViewById(R.id.button_reject_invitation);
        rejectButton.setOnClickListener(rejectButtonOnClickListener);

        invitationTextView.setText(String.format(getResources().getString(R.string.message_respond_vote), organizerEmail, eventDateTime));

        return view;
    }

    private View.OnClickListener acceptButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int selectedId = restaurantRadioGroup.getCheckedRadioButtonId();

            if (!StringUtil.isNullOrEmpty(email) && selectedId != -1) {
                String[] passings = {Constants.POST_CUSTOMER_VOTING, String.valueOf(selectedId), idTempReserve, email};
                VotingHttp task = new VotingHttp();
                task.execute(passings);
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.error_submission), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener rejectButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!StringUtil.isNullOrEmpty(email)) {
                String[] passings = {Constants.POST_CUSTOMER_VOTING, "0", idTempReserve, email};
                VotingHttp task = new VotingHttp();
                task.execute(passings);
            }
        }
    };

    private class VotingHttp extends AsyncTask<String[], Void, Void> {

        ProgressDialog progressDialog;
        private JSONParser jsonParser = new JSONParser();
        private JSONObject invitationJsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Submitting. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String[]... strings) {
            String[] passed = strings[0]; // get passed array

            List<NameValuePair> paramsOrganizer = new ArrayList<NameValuePair>();
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, passed[1]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_VOTE_RESERVE, passed[2]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_EMAIL, passed[3]));
            invitationJsonObject = jsonParser.makeHttpRequest(passed[0], "POST", paramsOrganizer);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = invitationJsonObject.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    String message = invitationJsonObject.getString(Constants.TAG_MESSAGE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

                } else if (success == 1) {
                    // Clear all back stack and show events tab
                    int backStackCount = getFragmentManager().getBackStackEntryCount();
                    for (int i = 0; i < backStackCount; i++) {
                        // todo: pop to the correct pager
                        // Get the back stack fragment id.
                        int backStackId = getFragmentManager().getBackStackEntryAt(i).getId();
                        getFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class RestaurantHttp extends AsyncTask<String[], Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject restaurantJsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String[]... strings) {
            String[] passed = strings[0]; // get passed array

            List<NameValuePair> paramsOrganizer = new ArrayList<NameValuePair>();
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, passed[1]));
            restaurantJsonObject = jsonParser.makeHttpRequest(passed[0], "GET", paramsOrganizer);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = restaurantJsonObject.getInt(Constants.TAG_SUCCESS);

                if (success == 0) {
                    String message = restaurantJsonObject.getString(Constants.TAG_MESSAGE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    JSONObject foodAttributes;
                    JSONArray foodItems = restaurantJsonObject.getJSONArray(Constants.TAG_RESULT);
                    for (int i = 0; i < foodItems.length(); i++) {
                        foodAttributes = foodItems.getJSONObject(i);

                        String name = foodAttributes.getString(Constants.TAG_RESTAURANT_NAME);

                        switch (count){
                            case 0:
                                radioButtonOne.setText(name);
                                count++;

                                String[] passing = new String[2];
                                passing[0] = Constants.GET_RESTAURANT_INFO;
                                passing[1] = idRestaurant[1];
                                RestaurantHttp task = new RestaurantHttp();
                                task.execute(passing);

                                break;
                            case 1:
                                radioButtonTwo.setText(name);
                                break;
                        }
                    }

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
