package com.gavin.eat2gether.model;

import android.graphics.Bitmap;
import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by gavin on 26/07/2016.
 */
public class Restaurant {

    private int idRestaurant;
    private String restaurantName;
    private String type;
    private String photo;
    private double distance;

    private String address;
    private String operatingHour;
    private String website;
    private String phone;

    public Restaurant(int idRestaurant, String restaurantName, String type, String photo, double distance,
                      String address, String operatingHour, String website, String phone) {
        this.idRestaurant = idRestaurant;
        this.restaurantName = restaurantName;
        this.type = type;
        this.photo = photo;
        this.distance = distance;
        this.address = address;
        this.operatingHour = operatingHour;
        this.website = website;
        this.phone = phone;
    }

    public int getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(int idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOperatingHour() {
        return operatingHour;
    }

    public void setOperatingHour(String operatingHour) {
        this.operatingHour = operatingHour;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
