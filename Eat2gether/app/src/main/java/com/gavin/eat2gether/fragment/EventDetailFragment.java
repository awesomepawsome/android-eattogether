package com.gavin.eat2gether.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.model.Event;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.ImageUtil;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 28/07/2016.
 */
public class EventDetailFragment extends Fragment {

    Event event;

    View view;
    ImageView restaurantImage;
    TextView eventName, eventType, eventAddress, eventDate, eventPax, paxLabel, orderTitle, participantsTitle;
    LinearLayout participantsLayout, ordersLayout;
    Button orderButton;
    String email;
    private SharedPreferences sharedPreferences;
    //Manually add participants and orders (if ordered)
    private JSONObject jsonObjectEvent,jsonObjectAttendee, jsonObjectAccepted;
    private ProgressDialog progressDialog;
    boolean isConfirmedEvent = false;

    Bundle args;

    public EventDetailFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            Bundle args = getArguments();
            event = (Event) args.getSerializable(Constants.EVENT_OBJECT);
            Log.d(Constants.APP_NAME, "" + event.getRestaurantName());
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_customer_event_info, container, false);
        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        getActivity().getActionBar().setTitle(String.valueOf(event.getRestaurantName()));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        initView(view);

        email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL,null);
        RetrieveOrders retrieveOrders = new RetrieveOrders();
        RetrieveAttendees retrieveAttendees = new RetrieveAttendees();
        RetrieveAcceptedAttendees retrieveAcceptedAttendees = new RetrieveAcceptedAttendees();
        if(email != null && event.getIdReservation() != 0 && !event.getValidity().equals("1")){
            //Confirmed event
            retrieveOrders.execute(Constants.GET_CUSTOMER_ORDERS,String.valueOf(event.getIdReservation()),email);
            retrieveAttendees.execute(Constants.GET_EVENT_ATTENDEES,String.valueOf(event.getIdReservation()));
            isConfirmedEvent = true;
        }else if(!event.getRestaurantAddress().equals("") && event.getValidity().equals("1")){
            //Pending event
            retrieveAcceptedAttendees.execute(Constants.GET_ACCEPTED_ATTENDEES,String.valueOf(event.getIdReservation()));
            orderTitle.setVisibility(View.GONE);
            isConfirmedEvent = false;
        }
        Log.d(Constants.APP_NAME, "isConfirmedEvent: " + isConfirmedEvent);

        return view;
    }

    private void initView(View view){
        restaurantImage = (ImageView) view.findViewById(R.id.img_restaurant);
        eventName = (TextView) view.findViewById(R.id.tv_event_name);
        eventType = (TextView) view.findViewById(R.id.tv_event_type);
        eventAddress = (TextView) view.findViewById(R.id.tv_event_address);
        eventDate = (TextView) view.findViewById(R.id.tv_event_date);
        eventPax = (TextView) view.findViewById(R.id.tv_event_status);
        paxLabel = (TextView) view.findViewById(R.id.label_event_pax);
        orderButton = (Button) view.findViewById(R.id.btn_order_food);
        orderTitle = (TextView) view.findViewById(R.id.title_my_orders);
        participantsLayout = (LinearLayout) view.findViewById(R.id.layout_participants);
        participantsTitle = (TextView) view.findViewById(R.id.title_participants);
        ordersLayout = (LinearLayout) view.findViewById(R.id.layout_order_items);

        eventName.setText(event.getRestaurantName());
        eventType.setText(event.getRestaurantType());
        eventAddress.setText(event.getRestaurantAddress());
        eventDate.setText(event.getReserveDateTime().substring(0, (event.getReserveDateTime().length() - 3) ));
        if(event.getNumberOfPax() == 0){
            eventPax.setVisibility(View.GONE);
            paxLabel.setVisibility(View.GONE);
        }else{
            eventPax.setVisibility(View.VISIBLE);
            paxLabel.setVisibility(View.VISIBLE);
            eventPax.setText(String.valueOf(event.getNumberOfPax()));
        }

        Bitmap photoBitmap = ImageUtil.decodeBase64(event.getPhoto());
        if (photoBitmap == null) {
            Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
            photoBitmap = ((BitmapDrawable)drawable).getBitmap();
            restaurantImage.setImageBitmap(photoBitmap);
        } else {
            restaurantImage.setImageBitmap(photoBitmap);
        }

        orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    args = new Bundle();
                    args.putString(Constants.TAG_RESTAURANT_ID, String.valueOf(event.getRestaurantId()));
                    args.putString(Constants.TAG_RESTAURANT_NAME, String.valueOf(event.getRestaurantName()));
                    args.putString(Constants.TAG_EVENT_DATETIME, event.getReserveDateTime());
                    args.putString(Constants.TAG_ORGANIZER_EMAIL, email);
                    args.putBoolean(Constants.TAG_NEW_VOTE, false);
                    args.putInt(Constants.TAG_EVENT_ID, event.getIdReservation());

                    if(isConfirmedEvent){
                        args.putInt(Constants.TAG_EVENT_PAX,1);
                        args.putBoolean(Constants.TAG_NEW_ORDER,false);

                        Fragment toFragment = new FoodOrderFragment();
                        toFragment.setArguments(args);
                        getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment)
                                .addToBackStack(null).commit();
                    }else{
                        args.putInt(Constants.TAG_EVENT_PAX,event.getNumberOfPax()+1);
                        args.putBoolean(Constants.TAG_NEW_ORDER,true);
                        checkTable(String.valueOf(event.getRestaurantId()));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void checkTable(String selectedRestaurant) {
        String reserveSlot = event.getReserveDateTime();
        TablesHttp task = new TablesHttp();
        task.execute(Constants.GET_TABLE_AVAILABILITY, selectedRestaurant, String.valueOf(reserveSlot));
    }

    private class TablesHttp extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectTableAvailability;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_EVENT_DATETIME, strings[2]));
            jsonObjectTableAvailability = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectTableAvailability.getInt(Constants.TAG_SUCCESS);
                String message = jsonObjectTableAvailability.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_late), Toast.LENGTH_SHORT).show();
                    // notify other invited participant of full house restaurant
                    Log.d(Constants.APP_NAME, "" + event.getIdReservation());
                    NotifyFullHouse notificationTask = new NotifyFullHouse();
                    notificationTask.execute(Constants.POST_CUSTOMER_INVITE_REJECTED, String.valueOf(event.getIdReservation()));
                } else if (success == 1) {
                    Fragment toFragment = new FoodOrderFragment();
                    toFragment.setArguments(args);
                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment)
                            .addToBackStack(null).commit();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class NotifyFullHouse extends AsyncTask<String, Void, Void>{
        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> paramsNotification = new ArrayList<NameValuePair>();
            paramsNotification.add(new BasicNameValuePair(Constants.TAG_TEMP_RESERVE, strings[1]));
            JSONParser.makeHttpRequest(strings[0], "POST", paramsNotification);
            System.out.println("Params " + strings[0] + " " + strings[1]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // Clear all back stack and show events tab
            int backStackCount = getFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < backStackCount; i++) {
                // todo: pop to the correct pager
                // Get the back stack fragment id.
                int backStackId = getFragmentManager().getBackStackEntryAt(i).getId();
                getFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    private class RetrieveOrders extends AsyncTask<String, String, String> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESERVATION_ID, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_EMAIL, strings[2]));
            jsonObjectEvent = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(String email) {
            super.onPostExecute(email);

            JSONObject orderAttributes;

            try {
                int success = jsonObjectEvent.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    orderButton.setVisibility(View.VISIBLE);
                    orderTitle.setVisibility(View.GONE);
                } else if (success == 1) {
                    orderButton.setVisibility(View.GONE);
                    orderTitle.setVisibility(View.VISIBLE);

                    JSONArray ordersArray = jsonObjectEvent.getJSONArray(Constants.TAG_RESULT);
                    for (int i = 0; i < ordersArray.length(); i++) {
                        orderAttributes = ordersArray.getJSONObject(i);
                        String foodName = orderAttributes.getString(Constants.TAG_FOOD_NAME);
                        int foodQuantity = orderAttributes.getInt(Constants.TAG_FOOD_QUANTITY);

                        //Programmatically add orders to layout
                        LinearLayout linearLayout = new LinearLayout(getActivity().getApplicationContext());

                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

                        TextView foodNameTextView = new TextView(getActivity().getApplicationContext());
                        TextView foodQuantityTextView = new TextView(getActivity().getApplicationContext());

                        foodNameTextView.setText(foodName);
                        foodNameTextView.setTextSize(16);
                        foodNameTextView.setTextColor(Color.BLACK);
                        foodNameTextView.setPadding(8,8,8,8);
                        foodNameTextView.setWidth(800);
                        foodQuantityTextView.setText(foodQuantity+"");
                        foodQuantityTextView.setTextSize(16);
                        foodQuantityTextView.setTextColor(Color.BLACK);
                        foodQuantityTextView.setPadding(8,8,8,8);

                        linearLayout.addView(foodNameTextView);
                        linearLayout.addView(foodQuantityTextView);

                        ordersLayout.addView(linearLayout);
                        ordersLayout.invalidate();
                    }

                } else {
                    orderButton.setVisibility(View.GONE);
                    orderTitle.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class RetrieveAttendees extends AsyncTask<String, String, String> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESERVATION_ID, strings[1]));
            jsonObjectAttendee = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(String email) {
            super.onPostExecute(email);

            JSONObject attendeeAttribute;

            try {
                int success = jsonObjectAttendee.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {

                } else if (success == 1) {

                    JSONArray attendees = jsonObjectAttendee.getJSONArray(Constants.TAG_RESULT);
                    for (int i = 0; i < attendees.length(); i++) {
                        attendeeAttribute = attendees.getJSONObject(i);
                        String attendeeEmail = attendeeAttribute.getString(Constants.TAG_ATTENDEE_EMAIL);

                        //Programmatically add participants to layout

                        TextView attendeeTextView = new TextView(getActivity().getApplicationContext());

                        attendeeTextView.setText(attendeeEmail);
                        attendeeTextView.setTextSize(16);
                        attendeeTextView.setTextColor(Color.BLACK);
                        attendeeTextView.setPadding(8,8,8,8);

                        participantsLayout.addView(attendeeTextView);
                        participantsLayout.invalidate();
                    }

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class RetrieveAcceptedAttendees extends AsyncTask<String, String, String> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESERVATION_ID, strings[1]));
            jsonObjectAccepted = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(String email) {
            super.onPostExecute(email);

            JSONObject attendeeAttribute;

            try {
                int success = jsonObjectAccepted.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    TextView attendeeTextView = new TextView(getActivity().getApplicationContext());

                    attendeeTextView.setText("No one has accepted yet");
                    attendeeTextView.setTextSize(16);
                    attendeeTextView.setTextColor(Color.BLACK);
                    attendeeTextView.setPadding(8,8,8,8);

                    participantsLayout.addView(attendeeTextView);
                    participantsLayout.invalidate();
                    orderButton.setVisibility(View.GONE);

                } else if (success == 1) {
                    participantsTitle.setVisibility(View.VISIBLE);
                    participantsTitle.setText("Attendees");
                    JSONArray attendees = jsonObjectAccepted.getJSONArray(Constants.TAG_RESULT);

                    for (int i = 0; i < attendees.length(); i++) {
                        attendeeAttribute = attendees.getJSONObject(i);
                        String attendeeEmail = attendeeAttribute.getString(Constants.TAG_ACCEPTED_ATTENDEE);

                        //Programmatically add participants to layout

                        TextView attendeeTextView = new TextView(getActivity().getApplicationContext());

                        attendeeTextView.setText(attendeeEmail);
                        attendeeTextView.setTextSize(16);
                        attendeeTextView.setTextColor(Color.BLACK);
                        attendeeTextView.setPadding(8,8,8,8);

                        participantsLayout.addView(attendeeTextView);
                        participantsLayout.invalidate();
                    }

                } else {
                    participantsLayout.setVisibility(View.GONE);
                    orderButton.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                //e.printStackTrace();
                Log.e("log_tag", "Error parsing data "+e.toString());
            }
        }
    }

}
