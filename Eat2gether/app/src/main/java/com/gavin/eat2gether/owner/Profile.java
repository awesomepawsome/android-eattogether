package com.gavin.eat2gether.owner;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;

/**
 * Created by gavin on 7/14/2016.
 */
public class Profile extends Fragment {

    //TODO: Add save button in edit profile
    private SharedPreferences sharedPreferences;
    private View rootView;
    private TextView nameTextView, categoryTextView, addressTextView, contactTextView, hoursTextView, websiteTextView;
    private EditText categoryEditText, addressEditText, contactEditText, hoursEditText, websiteEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_owner_profile, container, false);

        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
        getActivity().getActionBar().setTitle(getResources().getString(R.string.title_my_profile));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        nameTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_name);
        categoryTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_category);
//        categoryEditText = (EditText) rootView.findViewById(R.id.editText_category);
        addressTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_address);
        addressEditText = (EditText) rootView.findViewById(R.id.editText_address);
        contactTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_contact);
        contactEditText = (EditText) rootView.findViewById(R.id.editText_contact);
        hoursTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_opening);
        hoursEditText = (EditText) rootView.findViewById(R.id.editText_openinghours);
        websiteTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_website);
        websiteEditText = (EditText) rootView.findViewById(R.id.editText_website);
        Button editProfileButton = (Button) rootView.findViewById(R.id.button_edit_profile);

        setUpOwnerProfile();
//        editProfileButton.setOnClickListener(editProfileListener);

        return rootView;
    }

    private void setUpOwnerProfile() {
        nameTextView.setText(sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_NAME, null));
        categoryTextView.setText(sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_TYPE, null));
        addressTextView.setText(sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_ADDRESS, null));
        contactTextView.setText(sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_PHONE, null));
        hoursTextView.setText(sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_OPERATING_HOURS, null));
        websiteTextView.setText(sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_WEBSITE, null));
    }

//    private OnClickListener editProfileListener = new OnClickListener() {
//        @Override
//        public void onClick(View view) {
////            categoryTextView.setVisibility(View.GONE);
////            categoryEditText.setVisibility(View.VISIBLE);
//            addressTextView.setVisibility(View.GONE);
//            addressEditText.setVisibility(View.VISIBLE);
//            contactTextView.setVisibility(View.GONE);
//            contactEditText.setVisibility(View.VISIBLE);
//            hoursTextView.setVisibility(View.GONE);
//            hoursEditText.setVisibility(View.VISIBLE);
//            websiteTextView.setVisibility(View.GONE);
//            websiteEditText.setVisibility(View.VISIBLE);
//        }
//    };


}
