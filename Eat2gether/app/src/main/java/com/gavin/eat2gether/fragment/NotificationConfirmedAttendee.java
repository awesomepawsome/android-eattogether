package com.gavin.eat2gether.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gavin.eat2gether.R;

/**
 * Created by gavin on 02/08/2016.
 */
public class NotificationConfirmedAttendee extends Fragment {

    View view;
    LinearLayout attendeesLayout;
    Button orderButton;

    public NotificationConfirmedAttendee(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification_confirmed_attendee, container, false);

        attendeesLayout = (LinearLayout) view.findViewById(R.id.layout_attendees);
        orderButton = (Button) view.findViewById(R.id.button_order_food);

        return view;
    }

}
