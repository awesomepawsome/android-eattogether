package com.gavin.eat2gether.model;

import java.io.Serializable;

/**
 * Created by gavin on 27/07/2016.
 */
public class Event implements Serializable {
    private int idReservation;
    private String reserveDateTime;
    private int numberOfPax;
    private String organizer;
    private int restaurantId;
    private String restaurantName;
    private String restaurantType;
    private String restaurantAddress;
    private String validity;
    private String photo;

    public Event(int idReservation, String reserveDateTime, int numberOfPax, String organizer,
                 int restaurantId, String restaurantName, String restaurantType, String restaurantAddress, String validity, String photo){
        this.idReservation = idReservation;
        this.reserveDateTime = reserveDateTime;
        this.numberOfPax = numberOfPax;
        this.organizer = organizer;
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
        this.restaurantType = restaurantType;
        this.restaurantAddress = restaurantAddress;
        this.validity = validity;
        this.photo = photo;
    }

    public int getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(int idReservation) {
        this.idReservation = idReservation;
    }

    public String getReserveDateTime() {
        return reserveDateTime;
    }

    public void setReserveDateTime(String reserveDateTime) {
        this.reserveDateTime = reserveDateTime;
    }

    public int getNumberOfPax() {
        return numberOfPax;
    }

    public void setNumberOfPax(int numberOfPax) {
        this.numberOfPax = numberOfPax;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantType() {
        return restaurantType;
    }

    public void setRestaurantType(String restaurantType) {
        this.restaurantType = restaurantType;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(int restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
