package com.gavin.eat2gether.application;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by gavin on 31/07/2016.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //Initializing firebase
        Firebase.setAndroidContext(getApplicationContext());
    }

}
