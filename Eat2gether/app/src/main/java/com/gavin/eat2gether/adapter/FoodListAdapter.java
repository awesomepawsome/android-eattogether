package com.gavin.eat2gether.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.fragment.FoodOrderFragment;
import com.gavin.eat2gether.fragment.RestaurantDetailFragment;
import com.gavin.eat2gether.model.Food;
import com.gavin.eat2gether.model.Restaurant;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.ImageUtil;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class FoodListAdapter extends BaseAdapter {

    Context context;
    private FragmentManager mFragmentManager;
    private List<Food> foods;

    public FoodListAdapter(Context context, FragmentManager mFragmentManager, List<Food> foods) {
        this.context = context;
        this.foods = foods;
        this.mFragmentManager = mFragmentManager;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int i) {
        return foods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return foods.get(i).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        FoodHolder holder = new FoodHolder();

        // First let's verify the convertView is not null
        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_food_menu, null);
            // Now we can fill the layout with the right values
            ImageView foodImageView = (ImageView) convertView.findViewById(R.id.image_food);
            TextView nameTextView = (TextView) convertView.findViewById(R.id.tv_food_name);
            TextView priceTextView = (TextView) convertView.findViewById(R.id.tv_food_price);
            TextView quantityTextView = (TextView) convertView.findViewById(R.id.label_quantity_number);
            Button subButton = (Button) convertView.findViewById(R.id.button_sub_quantity);
            Button addButton = (Button) convertView.findViewById(R.id.button_add_quantity);

            holder.foodImageView = foodImageView;
            holder.nameTextView = nameTextView;
            holder.priceTextView = priceTextView;
            holder.quantityTextView = quantityTextView;
            holder.subButton = subButton;
            holder.addButton = addButton;

            convertView.setTag(holder);
        } else {
            holder = (FoodHolder) convertView.getTag();
        }

        final Food food = foods.get(position);
        if (food != null) {
            // round to 2 decimals
            DecimalFormat decimalFormat = new DecimalFormat("####0.00");
            decimalFormat.setRoundingMode(RoundingMode.CEILING);

            Bitmap photoBitmap = ImageUtil.decodeBase64(food.getPhoto());
            if (photoBitmap == null) {
                Drawable drawable = context.getResources().getDrawable(R.mipmap.ic_launcher);
                photoBitmap = ((BitmapDrawable)drawable).getBitmap();
                holder.foodImageView.setImageBitmap(photoBitmap);
            } else {
                holder.foodImageView.setImageBitmap(photoBitmap);
            }

            holder.nameTextView.setText(food.getName());
            holder.priceTextView.setText("RM " + decimalFormat.format(food.getPrice()));
            final TextView quantityTextView = holder.quantityTextView;

            final int value = Integer.parseInt(quantityTextView.getText().toString());
            holder.subButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (value > 0) {
                        int newValue = value - 1;
                        quantityTextView.setText(String.valueOf(newValue));
                        notifyDataSetChanged();
                        FoodOrderFragment.updateOrderedFood(food.getIdFood(), newValue, - + food.getPrice());
                    }
                }
            });

            holder.addButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int newValue = value + 1;
                    quantityTextView.setText(String.valueOf(newValue));
                    notifyDataSetChanged();
                    FoodOrderFragment.updateOrderedFood(food.getIdFood(), newValue, food.getPrice());
                }
            });
        }

        return convertView;
    }

    private static class FoodHolder {
        public ImageView foodImageView;
        public TextView nameTextView, priceTextView, quantityTextView;
        public Button subButton, addButton;
    }


}
