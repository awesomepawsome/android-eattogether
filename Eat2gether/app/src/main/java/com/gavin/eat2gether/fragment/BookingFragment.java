package com.gavin.eat2gether.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.StringUtil;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BookingFragment extends Fragment {

    Calendar myCalendar = Calendar.getInstance();

    String idrestaurant, restaurantName, operatingHour;

    LinearLayout addFriendLinearLayout;
    Button datePickerButton, timePickerButton, subButton, addButton, submitButton;
    TextView paxTextView, statusTextView;

    private ProgressDialog progressDialog;

    public BookingFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        idrestaurant = args.getString(Constants.TAG_RESTAURANT_ID);
        operatingHour = args.getString(Constants.TAG_RESTAURANT_OPERATING_HOURS);
        restaurantName = args.getString(Constants.TAG_RESTAURANT_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_bookings, container, false);

        initView(rootView);

        getActivity().getActionBar().setTitle(getResources().getString(R.string.label_restaurant));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        return rootView;
    }

    private void initView(View rootView) {
        datePickerButton = (Button) rootView.findViewById(R.id.button_booking_date);
        datePickerButton.setOnClickListener(datePickerButtonOnClickListener);

        timePickerButton = (Button) rootView.findViewById(R.id.button_booking_time);
        timePickerButton.setOnClickListener(timePickerButtonOnClickListener);

        paxTextView = (TextView) rootView.findViewById(R.id.label_booking_number);
        subButton = (Button) rootView.findViewById(R.id.button_sub_pax);
        addButton = (Button) rootView.findViewById(R.id.button_add_pax);
        subButton.setOnClickListener(subAddOnClickListener);
        addButton.setOnClickListener(subAddOnClickListener);

        statusTextView = (TextView) rootView.findViewById(R.id.label_status_result);

        addFriendLinearLayout = (LinearLayout) rootView.findViewById(R.id.row_add_friend);

        submitButton = (Button) rootView.findViewById(R.id.btn_invite_friends);
        submitButton.setOnClickListener(submitButtonOnClickListener);
    }

    private View.OnClickListener datePickerButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(getContext(), date,
                    myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();

        }
    };

    private View.OnClickListener timePickerButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new TimePickerDialog(getContext(), time,
                    myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE),
                    DateFormat.is24HourFormat(getContext())).show();
        }
    };

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateLabel();
        }
    };

    TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            myCalendar.set(Calendar.MINUTE, minute);
            updateTimeLabel();
        }
    };

    private View.OnClickListener subAddOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int value = Integer.parseInt(paxTextView.getText().toString());
            switch (view.getId()){
                case R.id.button_sub_pax:
                    if (value > 1 ){
                        value =  value - 1;
                        paxTextView.setText(String.valueOf(value));
                        addFriendLinearLayout.removeView(addFriendLinearLayout.getChildAt(value - 1));
                    }
                    break;
                case R.id.button_add_pax:
                    if (value == 15 ){
                        Toast.makeText(getContext(), getResources().getString(R.string.error_max),
                                Toast.LENGTH_SHORT).show();
                    }else {
                        value =  value + 1;
                        paxTextView.setText(String.valueOf(value));

                        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                        View emailView = layoutInflater.inflate(R.layout.item_friends_field, null, false);
                        addFriendLinearLayout.addView(emailView);
                    }
                    break;
            }
        }
    };

    private View.OnClickListener submitButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean isValidEmailInput = true;
            String available =  getActivity().getResources().getString(R.string.label_status_available);
            int count = addFriendLinearLayout.getChildCount();
            String[] tempEmails = new String[count];

            String dateTime = datePickerButton.getText().toString() + " " + timePickerButton.getText().toString() + ":00";
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG,
                    Context.MODE_PRIVATE);
            String email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL,null);

            if (count == 0){
                if (statusTextView.getText().toString().equals(available)){
                    try {
                        Bundle args = new Bundle();
                        args.putString(Constants.TAG_RESTAURANT_ID, idrestaurant);
                        args.putString(Constants.TAG_RESTAURANT_NAME, restaurantName);
                        args.putString(Constants.TAG_EVENT_DATETIME, dateTime);
                        args.putString(Constants.TAG_ORGANIZER_EMAIL, email);
                        args.putInt(Constants.TAG_EVENT_PAX,1);
                        args.putInt(Constants.TAG_EVENT_ID,0);
                        args.putBoolean(Constants.TAG_NEW_ORDER,true);
                        args.putBoolean(Constants.TAG_NEW_VOTE,false);

                        Fragment toFragment = new FoodOrderFragment();
                        toFragment.setArguments(args);
                        getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment)
                                .addToBackStack(null).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return;
            } else {
                for (int i=0;  i < count; i++){
                    View v = addFriendLinearLayout.getChildAt(i);
                    if (v instanceof EditText) {
                        tempEmails[i] = ((EditText) v).getText().toString();
                        if (!StringUtil.isValidEmail(tempEmails[i])){
                            isValidEmailInput = false;
                            break;
                        }
                    }
                }
            }

            if (isValidEmailInput && statusTextView.getText().toString().equals(available)){

                String[] organizer = {Constants.POST_CUSTOMER_TEMP_RESERVE_ORGANIZER, dateTime, idrestaurant, email};

                // combine invitation web services with arrays invited email
                String[] tempInvited = {Constants.POST_CUSTOMER_TEMP_RESERVE_INVITED};
                String[] invited= new String[tempInvited.length + tempEmails.length];
                System.arraycopy(tempInvited, 0, invited, 0, tempInvited.length);
                System.arraycopy(tempEmails, 0, invited, tempInvited.length, tempEmails.length);

                // combine two arrays of web services
                int organizerLength = organizer.length;
                int invitedLength = invited.length;
                String[] passings= new String[organizerLength + invitedLength];
                System.arraycopy(organizer, 0, passings, 0, organizerLength);
                System.arraycopy(invited, 0, passings, organizerLength, invitedLength);

                TempBookingHttp task = new TempBookingHttp();
                task.execute(passings);

                // send notifications
                BookingNotification notificationTask = new BookingNotification();
                notificationTask.execute(Constants.POST_NOTIFICATION_BOOKING, email);

            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.error_submission),
                        Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void updateDateLabel() {
        // YYYY-MM-DD HH:mm:ss (e.g. 2016-07-13 00:00:00)
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        datePickerButton.setText(sdf.format(myCalendar.getTime()));
        checkSlot();
    }

    private void updateTimeLabel() {
        String myFormat = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        timePickerButton.setText(sdf.format(myCalendar.getTime()));
        checkSlot();
    }

    private void checkSlot() {
        boolean isDateSet = (datePickerButton.getText() != getResources().getText(R.string.btn_date_picker));
        boolean isTimeSet = (timePickerButton.getText() != getResources().getText(R.string.btn_time_picker));

        if (isDateSet && isTimeSet){
            // operating hour stored in db is default as 0900 - 2100 (24 hour format)
            String startTime = operatingHour.substring(0,2) + ":" + operatingHour.substring(2,4);
            String endTime = operatingHour.substring((operatingHour.indexOf('-') + 2), (operatingHour.indexOf('-') + 4))
                    + ":" + operatingHour.substring((operatingHour.indexOf('-') + 4), operatingHour.length());

            SimpleDateFormat parserDate = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat parserTime = new SimpleDateFormat("HH:mm");
            try {
                Date start = parserTime.parse(startTime);

                // does not accept booking 1 hour before close time
                Date end = parserTime.parse(endTime);
                end.setTime(end.getTime() - 3600 * 1000);

                Date inputDate = parserDate.parse(datePickerButton.getText().toString());
                Date inputTime = parserTime.parse(timePickerButton.getText().toString());
                if (inputTime.before(start) || inputTime.after(end) || inputDate.before(new Date())) {
                    statusTextView.setText(R.string.label_status_not_available);
                    Toast.makeText(getContext(), getResources().getString(R.string.error_close),
                            Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    checkTable();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return;
    }

    private void checkTable() {
        String reserveSlot = datePickerButton.getText() + " " + timePickerButton.getText() + ":00";
        TablesHttp task = new TablesHttp();
        task.execute(Constants.GET_TABLE_AVAILABILITY, idrestaurant, String.valueOf(reserveSlot));
    }

    private class TablesHttp extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectTableAvailability;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_EVENT_DATETIME, strings[2]));
            jsonObjectTableAvailability = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectTableAvailability.getInt(Constants.TAG_SUCCESS);
                String message = jsonObjectTableAvailability.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    statusTextView.setText(R.string.label_status_not_available);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    statusTextView.setText(R.string.label_status_available);
                } else {
                    statusTextView.setText(R.string.label_status_not_available);
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class TempBookingHttp extends AsyncTask<String[], Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject bookingJsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Submitting. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String[]... strings) {
            String[] passed = strings[0]; // get passed array

            List<NameValuePair> paramsOrganizer = new ArrayList<NameValuePair>();
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_TEMP_DATE_TIME, passed[1]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, passed[2]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_ORGANIZER_EMAIL, passed[3]));
            bookingJsonObject = jsonParser.makeHttpRequest(passed[0], "POST", paramsOrganizer);

            List<NameValuePair> paramsInvited = new ArrayList<NameValuePair>();
            // start with int = 5, because invited email list start at that point
            for(int i = 5; i < passed.length; i++) {
                paramsInvited.add(new BasicNameValuePair(Constants.TAG_INVITED_EMAIL, passed[i]));
                bookingJsonObject = jsonParser.makeHttpRequest(passed[4], "POST", paramsInvited);
                paramsInvited.remove(Constants.TAG_INVITED_EMAIL);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = bookingJsonObject.getInt(Constants.TAG_SUCCESS);
                String message = bookingJsonObject.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    // Clear all back stack and show events tab
                    int backStackCount = getFragmentManager().getBackStackEntryCount();
                    for (int i = 0; i < backStackCount; i++) {
                        // todo: pop to the correct pager
                        // Get the back stack fragment id.
                        int backStackId = getFragmentManager().getBackStackEntryAt(i).getId();
                        getFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class BookingNotification extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> paramsNotification = new ArrayList<NameValuePair>();
            paramsNotification.add(new BasicNameValuePair(Constants.TAG_ORGANIZER_EMAIL, strings[1]));
            JSONParser.makeHttpRequest(strings[0], "POST", paramsNotification);
            return null;
        }
    }

}
