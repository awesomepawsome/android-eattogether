package com.gavin.eat2gether.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.adapter.RestaurantListAdapter;
import com.gavin.eat2gether.model.Restaurant;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RestaurantFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    View view;
    Toolbar toolbar;
    EditText searchEditText;
    ListView restaurantsList;
    FloatingActionButton searchButton;

    RestaurantListAdapter adapter;
    List<Restaurant> restaurants = new ArrayList<Restaurant>();

    private ProgressDialog progressDialog;

    private static final String TAG = RestaurantFragment.class.getCanonicalName();
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    public RestaurantFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (checkPlayServices()) {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
            createLocationRequest();
            startLocationUpdate();
            displayLocation();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_customer_restaurants, container, false);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        searchEditText = (EditText) view.findViewById(R.id.edit_search);
        restaurantsList = (ListView) view.findViewById(R.id.list_restaurants);
        searchButton = (FloatingActionButton) view.findViewById(R.id.fab_restaurants_search);
        searchButton.setOnClickListener(searchButtonOnClickListener);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        //checkGpsSettings();

        return view;
    }

    private View.OnClickListener searchButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (searchEditText.getVisibility() == View.VISIBLE){
                searchEditText.setVisibility(View.GONE);
            }else{
                searchEditText.setVisibility(View.VISIBLE);
                searchEditText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);
            }
        }
    };

    public void checkGpsSettings() {
        LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getResources().getString(R.string.dialog_msg_gps_access))
                    .setCancelable(false)
                    .setPositiveButton(getResources().getString(R.string.action_yes), new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.action_no), new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            dialog.cancel();

                            //If Location Access is disable, pop alert message to user
                            try{
                                new AlertDialog.Builder(getActivity())
                                        .setTitle(getResources().getString(R.string.dialog_title_alert))
                                        .setMessage(getResources().getString(R.string.dialog_msg_no_gps))
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                //do nothing
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }
    }

    private void displayLocation() {
        final long SCAN_PERIOD = 10000; // Stops scanning after 10 seconds.
        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        }, SCAN_PERIOD);

        double latitude, longitude;
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
        } else {
            // todo: default as Utar Kampar location if location not found
            latitude = 4.339135;
            longitude = 101.140248;
        }
        Constants.mLatitude = latitude;
        Constants.mLongitude = longitude;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    protected void createLocationRequest() {
        final int LOC_UPDATE_INTERVAL = 10000;
        final int LOC_FASTEST_INTERVAL = 5000;
        final int LOC_DISPLACEMENT = 10;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOC_UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(LOC_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(LOC_DISPLACEMENT);
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.e(Constants.APP_NAME, "Play service fail");
            }
            return false;
        }
        return true;
    }

    protected void startLocationUpdate() {
        try {
            try {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    protected void stopLocationUpdate() {
        try {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: " + connectionResult.getErrorCode());
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("Restaurant onResume");
        RestaurantsHttp task = new RestaurantsHttp();
        task.execute(Constants.GET_NEAREST_RESTAURANT, String.valueOf(Constants.mLatitude), String.valueOf(Constants.mLongitude));
    }

    private class RestaurantsHttp extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectRestaurant;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_LATITUDE, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_LONGTITUDE, strings[2]));
            jsonObjectRestaurant = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // clear list view if navigate from other tabs
            if (adapter != null){
                restaurants.clear();
                adapter.notifyDataSetChanged();
            }

            JSONObject restaurantAttributes;

            try {
                int success = jsonObjectRestaurant.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    // No nearby restaurant
                } else if (success == 1) {
                    JSONArray restaurantItems = jsonObjectRestaurant.getJSONArray(Constants.TAG_RESERVATION_RESULTS);
                    for (int i = 0; i < restaurantItems.length(); i++) {
                        restaurantAttributes = restaurantItems.getJSONObject(i);

                        int idRestaurant = Integer.parseInt(restaurantAttributes.getString(Constants.TAG_RESTAURANT_ID));
                        String restaurantName = restaurantAttributes.getString(Constants.TAG_RESTAURANT_NAME);
                        String type = restaurantAttributes.getString(Constants.TAG_RESTAURANT_TYPE);
                        String photo = restaurantAttributes.getString(Constants.TAG_RESTAURANT_PHOTO);
                        double distance = Double.parseDouble(restaurantAttributes.getString(Constants.TAG_RESTAURANT_DISTANCE));
                        String address = restaurantAttributes.getString(Constants.TAG_RESTAURANT_ADDRESS);
                        String operatingHour = restaurantAttributes.getString(Constants.TAG_RESTAURANT_OPERATING_HOURS);
                        String website = restaurantAttributes.getString(Constants.TAG_RESTAURANT_WEBSITE);
                        String phone = restaurantAttributes.getString(Constants.TAG_RESTAURANT_PHONE);

                        restaurants.add(new Restaurant(idRestaurant, restaurantName, type, photo, distance,
                                address, operatingHour, website, phone));
                    }

                    try{
                        adapter = new RestaurantListAdapter(getContext(), getActivity().getSupportFragmentManager(), restaurants);
                        restaurantsList.setAdapter(adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    searchEditText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            Log.d("SearchOnTextChanged", "Text ["+s+"] - Start ["+start+"] - Before ["+before+"] - Count ["+count+"]");
                            if (count < before) {
                                // We're deleting char so we need to reset the adapter data
                                adapter.resetData();
                            }
                            adapter.getFilter().filter(s.toString());
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count,
                                                      int after) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

}
