package com.gavin.eat2gether.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by gavin on 7/22/2016.
 */
public class ImageUtil {

    public ImageUtil() {
    }

    public static String encodeToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] bytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        if (!StringUtil.isNullOrEmpty(input)) {
            byte[] decodedByte = Base64.decode(input, Base64.DEFAULT);
            return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
        } else {
            return null;
        }
    }
}
