package com.gavin.eat2gether.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.model.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 25/07/2016.
 */
public class EventsAdapter extends BaseAdapter{

    Context context;
    List<Event> events = new ArrayList<>();

    private static LayoutInflater inflater = null;

    public EventsAdapter(Context context, List<Event> events) {
        this.context = context;
        this.events = events;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return events.size();
    }

    @Override
    public Object getItem(int i) {
        return events.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = view;
        if(v == null)
            v = inflater.inflate(R.layout.item_event_list, null);

        TextView restaurantName = (TextView) v.findViewById(R.id.tv_event_name);
        TextView reservationDateTime = (TextView) v.findViewById(R.id.tv_event_datetime);
        TextView reservationStatus = (TextView) v.findViewById(R.id.tv_event_status);

        if(events.get(i).getRestaurantAddress().equals("")){
            restaurantName.setText("Restaurant Voting");
        }else{
            restaurantName.setText(events.get(i).getRestaurantName());
        }

        reservationDateTime.setText(events.get(i).getReserveDateTime());
        String status = "Pending";
        if(events.get(i).getValidity().equals("1")){
            status = "Pending";
        }else{
            status = "Confirmed";
        }
        reservationStatus.setText(status);

        return v;
    }
}
