package com.gavin.eat2gether.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toolbar;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.fragment.CustomerHomeFragment;
import com.gavin.eat2gether.fragment.VotingFragment;
import com.gavin.eat2gether.owner.HomeScreen;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 7/14/2016.
 */
public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private Toolbar toolbar;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        extras = getIntent().getExtras();
        if (extras != null && extras.getInt("LOGIN_ROLE") == R.id.radioButton_owner) {
            displayOwnerMainFragment();
        } else {
            sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
            String token = sharedPreferences.getString("token", null);
            String email = sharedPreferences.getString("firebaseEmail", null);
            updateToken task = new updateToken();
            task.execute(token, email);

        }

        initToolbar();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
    }

    private void displayOwnerMainFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeScreen()).commit();
    }

    private void displayCustomerMainFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new CustomerHomeFragment()).commit();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.showOverflowMenu();
        setActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (extras != null && extras.getInt(Constants.INTENT_EXTRAS_LOGIN_ROLE) == R.id.radioButton_owner) {
            getMenuInflater().inflate(R.menu.menu_owner, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_overflow, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_poll:
                Fragment toFragment = new VotingFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.container, toFragment)
                        .addToBackStack(null).commit();
                break;

            case R.id.action_profile:
                // pop up to show user profile
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

                String message = "Username: " + sharedPreferences.getString(Constants.SHARED_PREFERENCES_DISPLAY_NAME, null) +
                        "\nEmail: " + sharedPreferences.getString("firebaseEmail", null);
                alertDialogBuilder.setMessage(message);

                alertDialogBuilder.setNeutralButton("Ok",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                break;

            case R.id.action_logout:
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                this.finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private class updateToken extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("firebaseId", strings[0]));
            params.add(new BasicNameValuePair("email", strings[1]));
            JSONParser.makeHttpRequest(Constants.POST_CUSTOMER_FIREBASE_TOKEN, "POST", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            displayCustomerMainFragment();
        }
    }
}
