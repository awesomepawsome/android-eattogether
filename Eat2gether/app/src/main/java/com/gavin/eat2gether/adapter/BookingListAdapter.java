package com.gavin.eat2gether.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gavin on 7/22/2016.
 */
public class BookingListAdapter extends BaseAdapter {

    Context context;
    List<String> dataReservationId, dataName, dataPhone, dataEmail, dataPax, dataDate, dataTime;
    LinearLayout foodOrder;
    TextView orderTextView, quantityTextView;
    LinearLayout listFoodOrder;
    View item;

    private static LayoutInflater inflater = null;

    public BookingListAdapter(Context context, List<String> dataReservationId, List<String> dataName,
                              List<String> dataPhone, List<String> dataEmail, List<String> dataPax,
                              List<String> dataDate, List<String> dataTime) {
        this.context = context;
        this.dataReservationId = dataReservationId;
        this.dataName = dataName;
        this.dataPhone = dataPhone;
        this.dataEmail = dataEmail;
        this.dataPax = dataPax;
        this.dataDate = dataDate;
        this.dataTime = dataTime;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataReservationId.size();
    }

    @Override
    public Object getItem(int i) {
        return dataReservationId.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if (v == null)
            v = inflater.inflate(R.layout.fragment_owner_booking_details, null);
        TextView organizerTextView = (TextView) v.findViewById(R.id.tv_organizer_name);
        TextView phoneTextView = (TextView) v.findViewById(R.id.tv_organizer_contact);
        TextView emailTextView = (TextView) v.findViewById(R.id.tv_organizer_email);
        TextView noPaxTextView = (TextView) v.findViewById(R.id.tv_booking_pax);
        TextView bookingTimeTextView = (TextView) v.findViewById(R.id.tv_booking_datetime);
        foodOrder = (LinearLayout) v.findViewById(R.id.layout_booking_orders);

        listFoodOrder = (LinearLayout) v.findViewById(R.id.list_food_order);

        organizerTextView.setText(dataName.get(i));
        phoneTextView.setText(dataPhone.get(i));
        emailTextView.setText(dataEmail.get(i));
        noPaxTextView.setText(dataPax.get(i));
        bookingTimeTextView.setText(dataDate.get(i) + " | " + dataTime.get(i));

        new getFoodOrders().execute(dataReservationId.get(i));

        return v;
    }

    private class getFoodOrders extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectFoodOrder;
        List<String> foodNameList = new ArrayList<String>();
        List<String> foodQuantityList = new ArrayList<String>();

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESERVATION_ID, strings[0]));
            jsonObjectFoodOrder = jsonParser.makeHttpRequest(Constants.GET_RESERVATION_ORDER_URL, "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            JSONObject foodOrderAttributes;

            listFoodOrder.removeAllViews();

            try {
                int success = jsonObjectFoodOrder.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    foodOrder.setVisibility(View.GONE);
                } else {
                    JSONArray foodOrderItem = jsonObjectFoodOrder.getJSONArray(Constants.TAG_RESERVATION_RESULTS);

                    for (int i = 0; i < foodOrderItem.length(); i++) {
                        foodOrderAttributes = foodOrderItem.getJSONObject(i);
                        item = inflater.inflate(R.layout.item_food_order, null);
                        orderTextView = (TextView) item.findViewById(R.id.tv_order);
                        quantityTextView = (TextView) item.findViewById(R.id.tv_quantity);
                        foodNameList.add(foodOrderAttributes.getString(Constants.TAG_FOOD_NAME));
                        foodQuantityList.add(foodOrderAttributes.getString(Constants.TAG_FOOD_QUANTITY));
                        listFoodOrder.addView(item);
                        orderTextView.setText(foodNameList.get(i));
                        quantityTextView.setText(foodQuantityList.get(i));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
