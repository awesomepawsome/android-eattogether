package com.gavin.eat2gether.owner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.adapter.FoodMenuListAdapter;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.ImageUtil;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 7/14/2016.
 */
public class MenuScreen extends Fragment {

    // TODO: Refresh listview after adding item
    // TODO: Update food menu_overflow

    private SharedPreferences sharedPreferences;
    private JSONObject jsonObjectMenu;

    private ListView menuList;
    private TextView noFoodTextView;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_owner_menu, container, false);

        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        getActivity().getActionBar().setTitle(getResources().getString(R.string.title_menu));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton menuAddItemFloatingActionButton = (FloatingActionButton) rootView.findViewById(R.id.fab_menu_add_item);
        menuAddItemFloatingActionButton.setOnClickListener(menuAddItemListener);

        menuList = (ListView) rootView.findViewById(R.id.list_restaurant_menu);
        noFoodTextView = (TextView) rootView.findViewById(R.id.textView_missing_menu);

        displayMenu();

        menuList.setOnItemLongClickListener(menuItemListener);

        return rootView;
    }

    private OnClickListener menuAddItemListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, new MenuAddItem()).addToBackStack(null).commit();
        }
    };

    private OnItemLongClickListener menuItemListener = new OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
            TextView name = (TextView) view.findViewById(R.id.tv_food_name);
            TextView price = (TextView) view.findViewById(R.id.tv_food_price);
            ImageView photo = (ImageView) view.findViewById(R.id.img_food);

            sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_MENU_ID, name.getTag().toString()).apply();
            sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_MENU_NAME, name.getText().toString()).apply();
            sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_MENU_PRICE, price.getText().toString()).apply();

            if (photo.getTag() == null) {
                ImageUtil imageUtil = new ImageUtil();
                sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_MENU_PHOTO, imageUtil.encodeToBase64(((BitmapDrawable) photo.getDrawable()).getBitmap())).apply();
            }
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, new MenuUpdateItem()).addToBackStack(null).commit();
            return false;
        }
    };

    private void displayMenu() {
        String restaurantId = sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_ID, null);
        getMenu task = new getMenu();
        if (restaurantId != null) {
            task.execute(Constants.GET_FOOD_MENU_URL, restaurantId);
        }
    }

    private class getMenu extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            jsonObjectMenu = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            List<String> foodIdList = new ArrayList<String>();
            List<String> foodNameList = new ArrayList<String>();
            List<String> foodPriceList = new ArrayList<String>();
            List<String> foodPhotoList = new ArrayList<String>();
            JSONObject foodAttributes;

            try {
                int success = jsonObjectMenu.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    menuList.setVisibility(View.GONE);
                    noFoodTextView.setVisibility(View.VISIBLE);
                } else if (success == 1) {
                    menuList.setVisibility(View.VISIBLE);
                    noFoodTextView.setVisibility(View.GONE);

                    JSONArray foodItems = jsonObjectMenu.getJSONArray(Constants.TAG_FOOD_RESULT);
                    for (int i = 0; i < foodItems.length(); i++) {
                        foodAttributes = foodItems.getJSONObject(i);
                        foodIdList.add(foodAttributes.getString(Constants.TAG_FOOD_ID));
                        foodNameList.add(foodAttributes.getString(Constants.TAG_FOOD_NAME));
                        foodPriceList.add(foodAttributes.getString(Constants.TAG_FOOD_PRICE));
                        foodPhotoList.add(foodAttributes.getString(Constants.TAG_FOOD_PHOTO));
                    }

                    menuList.setAdapter(new FoodMenuListAdapter(getContext(), foodIdList, foodNameList, foodPriceList, foodPhotoList));

                } else {
                    menuList.setVisibility(View.GONE);
                    noFoodTextView.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
