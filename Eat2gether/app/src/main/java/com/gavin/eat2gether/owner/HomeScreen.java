package com.gavin.eat2gether.owner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 7/14/2016.
 */
public class HomeScreen extends Fragment {

    private SharedPreferences sharedPreferences;
    private JSONObject jsonObjectHome;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_owner_home, container, false);

        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        LinearLayout restaurantProfileTab = (LinearLayout) rootView.findViewById(R.id.layout_restaurant_profile);
        LinearLayout restaurantMenuTab = (LinearLayout) rootView.findViewById(R.id.layout_restaurant_menu);
        LinearLayout restaurantBookingTab = (LinearLayout) rootView.findViewById(R.id.layout_restaurant_booking);

        restaurantProfileTab.setOnClickListener(restaurantTabListener);
        restaurantMenuTab.setOnClickListener(restaurantTabListener);
        restaurantBookingTab.setOnClickListener(restaurantTabListener);

        if (!sharedPreferences.getBoolean("Saved", false)) {
            saveValueInSharedPreferences();
        }

        getActivity().getActionBar().setTitle(sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_NAME, ""));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);


        return rootView;
    }

    private OnClickListener restaurantTabListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.layout_restaurant_profile:
                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, new Profile()).addToBackStack(null).commit();
                    break;
                case R.id.layout_restaurant_menu:
                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, new MenuScreen()).addToBackStack(null).commit();
                    break;
                case R.id.layout_restaurant_booking:
                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, new Bookings()).addToBackStack(null).commit();
                    break;
            }
        }
    };

    private void saveValueInSharedPreferences() {
        String userEmail = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL, null);
        getRestaurantProfile task = new getRestaurantProfile();
        if (userEmail != null) {
            task.execute(Constants.GET_OWNER_RESTAURANT_URL, userEmail);
        }
    }

    private class getRestaurantProfile extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_EMAIL, strings[1]));
            jsonObjectHome = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectHome.getInt(Constants.TAG_SUCCESS);
                if (success == 1) {
                    JSONArray profileDetails = jsonObjectHome.getJSONArray(Constants.TAG_PROFILE);
                    JSONObject profileAttributes = profileDetails.getJSONObject(0);

                    List<String> profileKey = new ArrayList<>();
                    List<String> profileValue = new ArrayList<>();

                    profileKey.add(Constants.SHARED_PREFERENCES_RESTAURANT_ID);
                    profileKey.add(Constants.SHARED_PREFERENCES_RESTAURANT_NAME);
                    profileKey.add(Constants.SHARED_PREFERENCES_RESTAURANT_TYPE);
                    profileKey.add(Constants.SHARED_PREFERENCES_RESTAURANT_ADDRESS);
                    profileKey.add(Constants.SHARED_PREFERENCES_RESTAURANT_PHONE);
                    profileKey.add(Constants.SHARED_PREFERENCES_RESTAURANT_OPERATING_HOURS);
                    profileKey.add(Constants.SHARED_PREFERENCES_RESTAURANT_WEBSITE);

                    profileValue.add(profileAttributes.getString(Constants.TAG_RESTAURANT_ID));
                    profileValue.add(profileAttributes.getString(Constants.TAG_RESTAURANT_NAME));
                    profileValue.add(profileAttributes.getString(Constants.TAG_RESTAURANT_TYPE));
                    profileValue.add(profileAttributes.getString(Constants.TAG_RESTAURANT_ADDRESS));
                    profileValue.add(profileAttributes.getString(Constants.TAG_RESTAURANT_PHONE));
                    profileValue.add(profileAttributes.getString(Constants.TAG_RESTAURANT_OPERATING_HOURS));
                    profileValue.add(profileAttributes.getString(Constants.TAG_RESTAURANT_WEBSITE));

                    getActivity().getActionBar().setTitle(Constants.TAG_RESTAURANT_NAME);

                    for (int i = 0; i < profileValue.size(); i++) {
                        String temp = profileValue.get(i);
                        if (temp.equals("null")) {
                            temp = "-";
                            profileValue.set(i, temp);
                        }
                        sharedPreferences.edit().putString(profileKey.get(i), profileValue.get(i)).apply();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            sharedPreferences.edit().putBoolean("Saved", true).apply();

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
