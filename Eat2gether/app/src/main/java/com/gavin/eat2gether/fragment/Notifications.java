package com.gavin.eat2gether.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.adapter.NotificationListAdapter;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Notifications extends Fragment {

    private SharedPreferences sharedPreferences;
    private ListView notificationList;
    private TextView noResultTextView;

    public Notifications() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_customer_notifications, container, false);

        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
        notificationList = (ListView) rootView.findViewById(R.id.list_notifications);
        noResultTextView = (TextView) rootView.findViewById(R.id.tv_no_result);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("Notifications onResume");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            System.out.println("Notifications refresh");
            String email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL, null);
            NotificationHttp task = new NotificationHttp();
            if (email != null) {
                task.execute(Constants.GET_NOTIFICATION_INVITATIONS, email);
            }
        } else {

        }
    }

    private class NotificationHttp extends AsyncTask<String, Void, Void> {

        ProgressDialog progressDialog;
        JSONObject jsonObjectInvitation;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESERVATION_EMAIL, strings[1]));
            jsonObjectInvitation = JSONParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            List<String> idTempReserve = new ArrayList<>();
            List<String> restaurantNameList = new ArrayList<>();
            List<String> dateTimeList = new ArrayList<String>();
            List<String> organizerList = new ArrayList<String>();
            List<String> types = new ArrayList<String>();
            JSONObject invitationAttributes;

            try {
                int success = jsonObjectInvitation.getInt(Constants.TAG_SUCCESS);
                if (success == 1) {
                    JSONArray invitationItem = jsonObjectInvitation.getJSONArray(Constants.TAG_NOTIFICATION_INVITATION);

                    if (invitationItem.length() == 0){
                        notificationList.setVisibility(View.GONE);
                        noResultTextView.setVisibility(View.VISIBLE);
                    } else {
                        for (int i = 0; i < invitationItem.length(); i++) {
                            invitationAttributes = invitationItem.getJSONObject(i);

                            idTempReserve.add(invitationAttributes.getString(Constants.TAG_TEMP_RESERVE));
                            restaurantNameList.add(invitationAttributes.getString(Constants.TAG_RESTAURANT_NAME));
                            dateTimeList.add(invitationAttributes.getString(Constants.TAG_TEMP_DATE_TIME));
                            organizerList.add(invitationAttributes.getString(Constants.TAG_CUSTOMER_ORGANIZER));
                            types.add(invitationAttributes.getString(Constants.TAG_NOTI_TYPE));
                        }

                        notificationList.setAdapter(new NotificationListAdapter(getContext(), idTempReserve, types, restaurantNameList, dateTimeList,
                                organizerList, getActivity().getSupportFragmentManager()));

                        notificationList.setVisibility(View.VISIBLE);
                        noResultTextView.setVisibility(View.GONE);
                    }

                } else if (success == 0) {
                    notificationList.setVisibility(View.GONE);
                    noResultTextView.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
