package com.gavin.eat2gether.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.StringUtil;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationRespondEvent extends Fragment {

    View view;
    TextView invitationTextView;
    Button acceptButton;
    Button rejectButton;

    String email, idTempReserve, restaurantName, eventDateTime, organizerEmail;

    private SharedPreferences sharedPreferences;

    public NotificationRespondEvent() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Bundle args = getArguments();
            idTempReserve = args.getString(Constants.TAG_TEMP_RESERVE);
            restaurantName = args.getString(Constants.TAG_RESTAURANT_NAME);
            eventDateTime = args.getString(Constants.TAG_TEMP_DATE_TIME);
            organizerEmail = args.getString(Constants.TAG_ORGANIZER_EMAIL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification_respond_event, container, false);

        getActivity().getActionBar().setTitle("Event invitation");
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
        email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL, null);

        invitationTextView = (TextView) view.findViewById(R.id.tv_event_information);

        acceptButton = (Button) view.findViewById(R.id.button_accept_invitation);
        acceptButton.setOnClickListener(acceptButtonOnClickListener);

        rejectButton = (Button) view.findViewById(R.id.button_reject_invitation);
        rejectButton.setOnClickListener(rejectButtonOnClickListener);

        invitationTextView.setText(String.format(getResources().getString(R.string.message_respond_event), organizerEmail, eventDateTime, restaurantName));

        return view;
    }

    private View.OnClickListener acceptButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!StringUtil.isNullOrEmpty(email)) {
                //Update attendance
                String[] passings = {Constants.POST_CUSTOMER_INVITATION, "1", idTempReserve, email};
                InvitationHttp task = new InvitationHttp();
                task.execute(passings);

                //TODO: Notify organizer
                // send notifications
                NotifyOrganizer notificationTask = new NotifyOrganizer();
                notificationTask.execute(Constants.POST_NOTIFY_ORGANIZER, email, idTempReserve, "1");
            }
        }
    };

    private View.OnClickListener rejectButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!StringUtil.isNullOrEmpty(email)) {
                String[] passings = {Constants.POST_CUSTOMER_INVITATION, "0", idTempReserve, email};
                InvitationHttp task = new InvitationHttp();
                task.execute(passings);

                //TODO: Notify organizer
                // send notifications
                NotifyOrganizer notificationTask = new NotifyOrganizer();
                notificationTask.execute(Constants.POST_NOTIFY_ORGANIZER, email, idTempReserve, "0");
            }
        }
    };

    private class InvitationHttp extends AsyncTask<String[], Void, Void> {

        ProgressDialog progressDialog;
        private JSONParser jsonParser = new JSONParser();
        private JSONObject invitationJsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Submitting. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String[]... strings) {
            String[] passed = strings[0]; // get passed array

            List<NameValuePair> paramsOrganizer = new ArrayList<NameValuePair>();
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_ATTEND, passed[1]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_TEMP_RESERVE, passed[2]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_EMAIL, passed[3]));
            invitationJsonObject = jsonParser.makeHttpRequest(passed[0], "POST", paramsOrganizer);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = invitationJsonObject.getInt(Constants.TAG_SUCCESS);
                String message = invitationJsonObject.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    // Clear all back stack and show events tab
                    int backStackCount = getFragmentManager().getBackStackEntryCount();
                    for (int i = 0; i < backStackCount; i++) {
                        // todo: pop to the correct pager
                        // Get the back stack fragment id.
                        int backStackId = getFragmentManager().getBackStackEntryAt(i).getId();
                        getFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class NotifyOrganizer extends AsyncTask<String, Void, Void>{

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> paramsNotification = new ArrayList<NameValuePair>();
            paramsNotification.add(new BasicNameValuePair(Constants.TAG_RESPONDER_EMAIL, strings[1]));
            paramsNotification.add(new BasicNameValuePair(Constants.TAG_TEMP_RESERVE, strings[2]));
            paramsNotification.add(new BasicNameValuePair(Constants.TAG_RESPOND_STATUS, strings[3]));
            JSONParser.makeHttpRequest(strings[0], "GET", paramsNotification);
            System.out.println("Params " + strings[0] + " " + strings[1] + " " + strings[2] + " " + strings[3] + "");
            return null;
        }
    }
}
