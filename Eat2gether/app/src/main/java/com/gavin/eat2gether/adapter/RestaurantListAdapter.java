package com.gavin.eat2gether.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.fragment.CustomerHomeFragment;
import com.gavin.eat2gether.fragment.RestaurantDetailFragment;
import com.gavin.eat2gether.model.Restaurant;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.ImageUtil;
import com.gavin.eat2gether.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class RestaurantListAdapter extends BaseAdapter implements Filterable {

    Context context;
    private FragmentManager mFragmentManager;
    private List<Restaurant> restaurants;
    private List<Restaurant> oriRestaurants;
    private Filter restaurantFilter;

    public RestaurantListAdapter(Context context, FragmentManager mFragmentManager, List<Restaurant> restaurants) {
        this.context = context;
        this.restaurants = restaurants;
        this.mFragmentManager = mFragmentManager;
        this.oriRestaurants = restaurants;
    }

    @Override
    public int getCount() {
        return restaurants.size();
    }

    @Override
    public Object getItem(int i) {
        return restaurants.get(i);
    }

    @Override
    public long getItemId(int i) {
        return restaurants.get(i).hashCode();
    }

    private static class RestaurantHolder {
        public TextView nameTextView;
        public TextView typeTextView;
        public TextView distanceTextView;
        public ImageView iconImageView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        RestaurantHolder holder = new RestaurantHolder();

        // First let's verify the convertView is not null
        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_restaurant_list, null);
            // Now we can fill the layout with the right values
            TextView nameTextView = (TextView) convertView.findViewById(R.id.tv_restaurant_name);
            TextView typeTextView = (TextView) convertView.findViewById(R.id.tv_restaurant_type);
            TextView distanceTextView = (TextView) convertView.findViewById(R.id.tv_restaurant_distance);
            ImageView iconImageView = (ImageView) convertView.findViewById(R.id.img_restaurant);

            holder.nameTextView = nameTextView;
            holder.typeTextView = typeTextView;
            holder.distanceTextView = distanceTextView;
            holder.iconImageView = iconImageView;


            convertView.setTag(holder);
        }
        else{
            holder = (RestaurantHolder) convertView.getTag();
        }

        final Restaurant restaurant = restaurants.get(position);
        if (restaurant != null) {
            holder.nameTextView.setText(restaurant.getRestaurantName());
            holder.typeTextView.setText(restaurant.getType());
            holder.distanceTextView.setText(Double.toString(restaurant.getDistance()) + " km");

            Bitmap photoBitmap = ImageUtil.decodeBase64(restaurant.getPhoto());
            if (photoBitmap == null) {
                Drawable drawable = context.getResources().getDrawable(R.mipmap.ic_launcher);
                photoBitmap = ((BitmapDrawable)drawable).getBitmap();
                holder.iconImageView.setImageBitmap(photoBitmap);
            } else {
                holder.iconImageView.setImageBitmap(photoBitmap);
            }

            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Bundle args = new Bundle();
                    args.putInt(Constants.TAG_RESTAURANT_ID, restaurant.getIdRestaurant());
                    args.putString(Constants.TAG_RESTAURANT_NAME, restaurant.getRestaurantName());
                    args.putString(Constants.TAG_RESTAURANT_TYPE, restaurant.getType());
                    args.putString(Constants.TAG_RESTAURANT_ADDRESS, restaurant.getAddress());
                    args.putString(Constants.TAG_RESTAURANT_PHONE, restaurant.getPhone());
                    args.putString(Constants.TAG_RESTAURANT_OPERATING_HOURS, restaurant.getOperatingHour());
                    args.putString(Constants.TAG_RESTAURANT_PHOTO, restaurant.getPhoto());

                    Fragment toFragment = new RestaurantDetailFragment();
                    toFragment.setArguments(args);
                    mFragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment, "RestaurantFragment")
                            .addToBackStack("RestaurantFragment").commit();
                }
            });

        }

        return convertView;
    }

    public void resetData() {
        restaurants = oriRestaurants;
    }

    /*
	 * We create our filter
	 */

    @Override
    public Filter getFilter() {
        if (restaurantFilter == null)
            restaurantFilter = new RestaurantFilter();

        return restaurantFilter;
    }

    private class RestaurantFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = oriRestaurants;
                results.count = oriRestaurants.size();
            } else {
                // We perform filtering operation
                List<Restaurant> nRestaurantList = new ArrayList<Restaurant>();

                for (Restaurant restaurant : restaurants) {
                    if (restaurant.getRestaurantName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
                        nRestaurantList.add(restaurant);
                }
                results.values = nRestaurantList;
                results.count = nRestaurantList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            // Now we have to inform the adapter about the new list filtered
//            if (results.count == 0)
//                notifyDataSetInvalidated();
//            else {
//                restaurants = (List<Restaurant>) results.values;
//                notifyDataSetChanged();
//            }
            restaurants = (List<Restaurant>) results.values;
            notifyDataSetChanged();

        }

    }

}
