package com.gavin.eat2gether.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.StringUtil;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VotingFragment extends Fragment {

    Calendar myCalendar = Calendar.getInstance();

    LinearLayout addFriendLinearLayout;
    TextView paxTextView;
    Button datePickerButton, timePickerButton, subButton, addButton, requestButton;
    Spinner restaurantOneSpinner, restaurantTwoSpinner;

    boolean isValidTable = false, isValidDate = false;

    int restaurantTableCount = 0;

    HashMap<String, String> spinnerMap = new HashMap<String, String>();

    public VotingFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_vote_request, container, false);

        initView(rootView);

        getActivity().getActionBar().setTitle("Vote request");
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        return rootView;
    }

    private void initView(View rootView) {
        datePickerButton = (Button) rootView.findViewById(R.id.button_booking_date);
        datePickerButton.setOnClickListener(datePickerButtonOnClickListener);

        timePickerButton = (Button) rootView.findViewById(R.id.button_booking_time);
        timePickerButton.setOnClickListener(timePickerButtonOnClickListener);

        paxTextView = (TextView) rootView.findViewById(R.id.label_booking_number);
        subButton = (Button) rootView.findViewById(R.id.button_sub_pax);
        addButton = (Button) rootView.findViewById(R.id.button_add_pax);
        subButton.setOnClickListener(subAddOnClickListener);
        addButton.setOnClickListener(subAddOnClickListener);

        restaurantOneSpinner = (Spinner) rootView.findViewById(R.id.spinner_vote_one);
        restaurantTwoSpinner = (Spinner) rootView.findViewById(R.id.spinner_vote_two);
        loadSpinnerData();

        addFriendLinearLayout = (LinearLayout) rootView.findViewById(R.id.row_add_friend);

        requestButton = (Button) rootView.findViewById(R.id.btn_vote_request);
        requestButton.setOnClickListener(requestButtonOnClickListener);
    }

    private void loadSpinnerData() {
        RestaurantsHttp task = new RestaurantsHttp();
        task.execute(Constants.GET_NEAREST_RESTAURANT, String.valueOf(Constants.mLatitude), String.valueOf(Constants.mLongitude));
    }

    private View.OnClickListener datePickerButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(getContext(), date,
                    myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();

        }
    };

    private View.OnClickListener timePickerButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new TimePickerDialog(getContext(), time,
                    myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE),
                    DateFormat.is24HourFormat(getContext())).show();
        }
    };

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateLabel();
        }
    };

    TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            myCalendar.set(Calendar.MINUTE, minute);
            updateTimeLabel();
        }
    };

    private void updateDateLabel() {
        // YYYY-MM-DD HH:mm:ss (e.g. 2016-07-13 00:00:00)
        String myFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        datePickerButton.setText(sdf.format(myCalendar.getTime()));
        checkSlot();
    }

    private void updateTimeLabel() {
        String myFormat = "HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        timePickerButton.setText(sdf.format(myCalendar.getTime()));
        checkSlot();
    }

    private void checkSlot() {
        boolean isDateSet = (datePickerButton.getText() != getResources().getText(R.string.btn_date_picker));
        boolean isTimeSet = (timePickerButton.getText() != getResources().getText(R.string.btn_time_picker));

        if (isDateSet && isTimeSet){
            // operating hour stored in db is default as 0900 - 2100 (24 hour format)
            String startTime = "09:00";
            String endTime = "21:00";

            SimpleDateFormat parserDate = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat parserTime = new SimpleDateFormat("HH:mm");
            try {
                Date start = parserTime.parse(startTime);

                // does not accept booking 1 hour before close time
                Date end = parserTime.parse(endTime);
                end.setTime(end.getTime() - 3600 * 1000);

                Date inputDate = parserDate.parse(datePickerButton.getText().toString());
                Date inputTime = parserTime.parse(timePickerButton.getText().toString());
                if (inputTime.before(start) || inputTime.after(end) || inputDate.before(new Date())) {
                    isValidDate = false;
                    Toast.makeText(getContext(), getResources().getString(R.string.error_close),
                            Toast.LENGTH_SHORT).show();
                    return;
                }else{
                    isValidDate = true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return;
    }

    private void checkTable(String restaurantName) {
        int tempId = 0;
        for (Map.Entry<String, String> e : spinnerMap.entrySet()) {
            if (e.getValue().equals(restaurantName)) {
                tempId = Integer.parseInt(e.getKey());
                break;
            }
        }
        String reserveSlot = datePickerButton.getText() + " " + timePickerButton.getText() + ":00";
        TablesHttp task = new TablesHttp();
        task.execute(Constants.GET_TABLE_AVAILABILITY, String.valueOf(tempId), String.valueOf(reserveSlot));
    }

    private View.OnClickListener subAddOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int value = Integer.parseInt(paxTextView.getText().toString());
            switch (view.getId()) {
                case R.id.button_sub_pax:
                    if (value > 1) {
                        value = value - 1;
                        paxTextView.setText(String.valueOf(value));
                        addFriendLinearLayout.removeView(addFriendLinearLayout.getChildAt(value - 1));
                    }
                    break;
                case R.id.button_add_pax:
                    if (value == 15) {
                        Toast.makeText(getContext(), getResources().getString(R.string.error_max),
                                Toast.LENGTH_SHORT).show();
                    } else {
                        value = value + 1;
                        paxTextView.setText(String.valueOf(value));

                        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                        View emailView = layoutInflater.inflate(R.layout.item_friends_field, null, false);
                        addFriendLinearLayout.addView(emailView);
                    }
                    break;
            }
        }
    };

    private View.OnClickListener requestButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // reset counter
            restaurantTableCount = 0;
            // check is the restaurant selection available
            String nameOne = restaurantOneSpinner.getSelectedItem().toString();
            checkTable(nameOne);
        }
    };

    private class RestaurantsHttp extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectRestaurant;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_LATITUDE, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_LONGTITUDE, strings[2]));
            jsonObjectRestaurant = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            JSONObject restaurantAttributes;
            String[] spinnerArray;

            try {
                int success = jsonObjectRestaurant.getInt(Constants.TAG_SUCCESS);

                if (success == 0) {
                    // No nearby restaurant
                } else if (success == 1) {
                    JSONArray restaurantItems = jsonObjectRestaurant.getJSONArray(Constants.TAG_RESERVATION_RESULTS);

                    spinnerArray = new String[restaurantItems.length()];
                    for (int i = 0; i < restaurantItems.length(); i++) {
                        restaurantAttributes = restaurantItems.getJSONObject(i);

                        String idRestaurant = restaurantAttributes.getString(Constants.TAG_RESTAURANT_ID);
                        String restaurantName = restaurantAttributes.getString(Constants.TAG_RESTAURANT_NAME);

                        spinnerMap.put(idRestaurant, restaurantName);
                        spinnerArray[i] = restaurantName;
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    restaurantOneSpinner.setAdapter(adapter);
                    restaurantTwoSpinner.setAdapter(adapter);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class TablesHttp extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectTableAvailability;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_EVENT_DATETIME, strings[2]));
            jsonObjectTableAvailability = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectTableAvailability.getInt(Constants.TAG_SUCCESS);

                if (success == 0) {
                    isValidTable = false;
                    Toast.makeText(getContext(), getResources().getString(R.string.label_status_not_available),
                            Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    // break the check after two restaurant checking
                    if (restaurantTableCount == 1){
                        boolean isValidEmailInput = true, isValidRestaurantChoice;
                        int count = addFriendLinearLayout.getChildCount();
                        String[] tempEmails = new String[count];
                        int idOne = 0, idTwo = 0;

                        String dateTime = datePickerButton.getText().toString() + " " + timePickerButton.getText().toString() + ":00";
                        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG,
                                Context.MODE_PRIVATE);
                        String email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL, null);

                        if (count == 0) {
                            isValidEmailInput = false;
                            Toast.makeText(getContext(), getResources().getString(R.string.error_submission),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            for (int i = 0; i < count; i++) {
                                View v = addFriendLinearLayout.getChildAt(i);
                                if (v instanceof EditText) {
                                    tempEmails[i] = ((EditText) v).getText().toString();
                                    if (!StringUtil.isValidEmail(tempEmails[i])) {
                                        isValidEmailInput = false;
                                        break;
                                    }
                                }
                            }
                        }

                        String nameOne = restaurantOneSpinner.getSelectedItem().toString();
                        String nameTwo = restaurantTwoSpinner.getSelectedItem().toString();
                        for (Map.Entry<String, String> e : spinnerMap.entrySet()) {
                            if (e.getValue().equals(nameOne)) {
                                idOne = Integer.parseInt(e.getKey());
                            }
                            if (e.getValue().equals(nameTwo)) {
                                idTwo = Integer.parseInt(e.getKey());
                            }
                            if (idOne != 0 && idTwo != 0){
                                break;
                            }
                        }
                        if (idOne == idTwo){
                            isValidRestaurantChoice = false;
                        } else {
                            isValidRestaurantChoice = true;
                        }

                        if (isValidTable && isValidDate && isValidEmailInput && isValidRestaurantChoice) {
                            String appendRestaurant = String.valueOf(idOne) + "," + String.valueOf(idTwo);
                            String[] organizer = {Constants.POST_CUSTOMER_VOTE_RESERVE_ORGANIZER, dateTime, appendRestaurant, email};

                            // combine voting web services with arrays invited email
                            String[] tempInvited = {Constants.POST_CUSTOMER_VOTE_RESERVE_INVITED};
                            String[] invited= new String[tempInvited.length + tempEmails.length];
                            System.arraycopy(tempInvited, 0, invited, 0, tempInvited.length);
                            System.arraycopy(tempEmails, 0, invited, tempInvited.length, tempEmails.length);

                            // combine two arrays of web services
                            int organizerLength = organizer.length;
                            int invitedLength = invited.length;
                            String[] passings= new String[organizerLength + invitedLength];
                            System.arraycopy(organizer, 0, passings, 0, organizerLength);
                            System.arraycopy(invited, 0, passings, organizerLength, invitedLength);

                            for (int i = 0; i < passings.length; i++) {
                                Log.d(Constants.APP_NAME, passings[i]);
                            }

                            VoteHttp task = new VoteHttp();
                            task.execute(passings);

                        } else {
                            Log.e(Constants.APP_NAME, "isValidTable: " + isValidTable);
                            Log.e(Constants.APP_NAME, "isValidDate: " + isValidDate);
                            Log.e(Constants.APP_NAME, "isValidEmailInput: " + isValidEmailInput);
                            Log.e(Constants.APP_NAME, "isValidRestaurantChoice: " + isValidRestaurantChoice);

                            Toast.makeText(getContext(), getResources().getString(R.string.error_submission),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else if (restaurantTableCount < 1) {
                        isValidTable = true;
                        restaurantTableCount++;
                        String nameTwo = restaurantTwoSpinner.getSelectedItem().toString();
                        checkTable(nameTwo);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.error_others),
                                Toast.LENGTH_SHORT).show();
                    }

                } else {
                    isValidTable = false;
                    Toast.makeText(getContext(), getResources().getString(R.string.label_status_not_available),
                            Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    private class VoteHttp extends AsyncTask<String[], Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject voteJsonObject;
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Submitting. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String[]... strings) {
            String[] passed = strings[0]; // get passed array

            List<NameValuePair> paramsOrganizer = new ArrayList<NameValuePair>();
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_VOTE_DATE_TIME, passed[1]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, passed[2]));
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_ORGANIZER_EMAIL, passed[3]));
            voteJsonObject = jsonParser.makeHttpRequest(passed[0], "POST", paramsOrganizer);

            List<NameValuePair> paramsInvited = new ArrayList<NameValuePair>();
            // start with int = 5, because invited email list start at that point
            for(int i = 5; i < passed.length; i++) {
                paramsInvited.add(new BasicNameValuePair(Constants.TAG_INVITED_EMAIL, passed[i]));
                voteJsonObject = jsonParser.makeHttpRequest(passed[4], "POST", paramsInvited);
                paramsInvited.remove(Constants.TAG_INVITED_EMAIL);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = voteJsonObject.getInt(Constants.TAG_SUCCESS);
                String message = voteJsonObject.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    // Clear all back stack and show events tab
                    int backStackCount = getFragmentManager().getBackStackEntryCount();
                    for (int i = 0; i < backStackCount; i++) {
                        // todo: pop to the correct pager
                        // Get the back stack fragment id.
                        int backStackId = getFragmentManager().getBackStackEntryAt(i).getId();
                        getFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

}
