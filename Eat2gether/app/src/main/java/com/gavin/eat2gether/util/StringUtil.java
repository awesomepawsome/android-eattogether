package com.gavin.eat2gether.util;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class StringUtil {

    public static boolean isNullOrEmpty(String str) {
        return (str == null || str.trim().equals(""));
    }

    public static String combineStrInArr(List<String> arr) {
        String result = "";

        if (arr != null && arr.size() > 0) {
            for (String str : arr) {
                result += str;
            }
        }

        return result;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String parseScanDataVersionNo(String value) {
        if (!StringUtil.isNullOrEmpty(value) && value.length() == 4)
            return value.substring(0, 2) + "." + value.substring(2, 4);
        else
            return "";
    }

    public static String convertNullValueToZero(String value) {
        if (value == null)
            return "0";
        else
            return value;
    }

    public static boolean isPriceFormat(String input) {
        List<Pattern> format = new ArrayList<Pattern>();
        format.add(Pattern.compile("[rR][mM][\\d]+\\.[\\d]{1,2}"));
        format.add(Pattern.compile("[rR][mM][\\d]+"));
        format.add(Pattern.compile("\\d+"));
        format.add(Pattern.compile("[\\d]+\\.[\\d]{1,2}"));

        for (Pattern regex : format)
            if (regex.matcher(input).matches())
                return true;
        return false;
    }

    public static String removeAlphabetLeadingZero(String input) {
        input = input.replaceAll("[^\\d.]", "");
        return input.replaceFirst("^0+(?!$)", "");
    }
}
