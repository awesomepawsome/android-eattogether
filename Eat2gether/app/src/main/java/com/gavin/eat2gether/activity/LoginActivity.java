package com.gavin.eat2gether.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.firebase.MyFirebaseInstanceIDService;
import com.gavin.eat2gether.model.Event;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.StringUtil;
import com.gavin.eat2gether.webservice.JSONParser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 7/13/2016.
 */
public class LoginActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private RadioGroup loginRadioButtons;
    private RadioButton ownerRadioButton, customerRadioButton;
    private EditText inputEmailEditText, inputPasswordEditText;
    private Button signUpButton;

    private ProgressDialog progressDialog;
    private JSONObject jsonObjectLogin;
    private Intent intent;
    private String loginEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        Firebase.setAndroidContext(this);
        FirebaseMessaging.getInstance().subscribeToTopic("test");
        FirebaseInstanceId.getInstance().getToken();

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        loginRadioButtons = (RadioGroup) findViewById(R.id.radioGroup);
        ownerRadioButton = (RadioButton) findViewById(R.id.radioButton_owner);
        customerRadioButton = (RadioButton) findViewById(R.id.radioButton_customer);
        inputEmailEditText = (EditText) findViewById(R.id.input_email);
        inputPasswordEditText = (EditText) findViewById(R.id.input_password);

        Button loginButton = (Button) findViewById(R.id.btn_login);
        signUpButton = (Button) findViewById(R.id.btn_signup);

        loginButton.setOnClickListener(loginButtonOnClickListener);
        signUpButton.setOnClickListener(signUpButtonOnClickListener);

        loginRadioButtons.setOnCheckedChangeListener(loginRadioGroupCheckedChangeListener);

        animateSplashScreen();
    }

    private void animateSplashScreen() {
        final RelativeLayout loginLayout = (RelativeLayout) findViewById(R.id.layout_login_details);
        loginLayout.setVisibility(View.GONE);
        final RelativeLayout imageIconLayout = (RelativeLayout) findViewById(R.id.layout_login_logo);
        final Animation animTranslate = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.translate);
        animTranslate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                loginLayout.setVisibility(View.VISIBLE);
                Animation animFade = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fade_in);
                loginLayout.startAnimation(animFade);
                signUpButton.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imageIconLayout.startAnimation(animTranslate);
            }
        }, 1000);
    }

    // Sign up button click listener
    private OnClickListener signUpButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        }
    };

    // Login button click listener
    private OnClickListener loginButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (inputEmailEditText.getText().toString().isEmpty() || inputPasswordEditText.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_missing_email_password), Toast.LENGTH_SHORT).show();
            } else if (loginRadioButtons.getCheckedRadioButtonId() == -1) {
                // Do something when no radio buttons are selected
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_missing_login_role), Toast.LENGTH_SHORT).show();
            } else if (!isConnected()) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_missing_internet_connection), Toast.LENGTH_SHORT).show();
            } else {
                loginEmail = inputEmailEditText.getText().toString();
                String loginPassword = inputPasswordEditText.getText().toString();
                getLoginData task = new getLoginData();
                intent = new Intent(LoginActivity.this, MainActivity.class);
                if (ownerRadioButton.isChecked()) {
                    intent.putExtra(Constants.INTENT_EXTRAS_LOGIN_ROLE, ownerRadioButton.getId());
                    task.execute(Constants.GET_OWNER_LOGIN_URL, loginEmail, loginPassword);
                } else {
                    intent.putExtra(Constants.INTENT_EXTRAS_LOGIN_ROLE, customerRadioButton.getId());
                    task.execute(Constants.GET_CUSTOMER_LOGIN_URL, loginEmail, loginPassword);
                }
            }
        }
    };

    private OnCheckedChangeListener loginRadioGroupCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i) {
                case R.id.radioButton_owner:
                    signUpButton.setVisibility(View.INVISIBLE);
                    break;
                case R.id.radioButton_customer:
                    signUpButton.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    private class getLoginData extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_EMAIL, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_PASSWORD, strings[2]));
            jsonObjectLogin = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectLogin.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_invalid_login),
                            Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    finish();
                    JSONArray loginItem = jsonObjectLogin.getJSONArray(Constants.TAG_PROFILE);

                    JSONObject loginAttributes = loginItem.getJSONObject(0);

                    String displayName = null;
                    if (customerRadioButton.isChecked()) {
                        displayName = loginAttributes.getString(Constants.TAG_DISPLAY_NAME);
                    }

                    if (customerRadioButton.isChecked()) {
                        sharedPreferences.edit().putString("firebaseEmail", loginEmail).apply();
                        if (!StringUtil.isNullOrEmpty(displayName)) {
                            sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_DISPLAY_NAME, displayName).apply();
                        }
                        String storedToken = sharedPreferences.getString("token", null);
                        if (storedToken == null) {
                            storedToken = FirebaseInstanceId.getInstance().getToken();
                            MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
                            myFirebaseInstanceIDService.sendRegistrationToServer(storedToken, loginEmail);
                        }
//                        String uniqueId = loginAttributes.getString(Constants.TAG_FIREBASE_UNIQUE_ID);
//                        sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_FIREBASE_ID, uniqueId).apply();
                    }
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_EMAIL, loginEmail).apply();
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
