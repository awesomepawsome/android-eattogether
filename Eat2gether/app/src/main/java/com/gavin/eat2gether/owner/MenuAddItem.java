package com.gavin.eat2gether.owner;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.ImageUtil;
import com.gavin.eat2gether.util.StringUtil;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 7/18/2016.
 */
public class MenuAddItem extends Fragment {

    private static final int GALLERY_REQUEST = 100;
    private static final int CAMERA_REQUEST = 101;

    private EditText foodNameEditText, foodPriceEditText;
    private ImageView addFoodImage;
    private TextView foodImageTextView;

    private SharedPreferences sharedPreferences;
    private JSONObject jsonObjectAddFood;

    private boolean hasImage = false;
    private ProgressDialog progressDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_owner_menu_add, container, false);

        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        getActivity().getActionBar().setTitle(getResources().getString(R.string.label_add_food));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        Button addFood = (Button) rootView.findViewById(R.id.btn_add);
        addFoodImage = (ImageView) rootView.findViewById(R.id.img_food_new);
        foodNameEditText = (EditText) rootView.findViewById(R.id.input_food_name);
        foodPriceEditText = (EditText) rootView.findViewById(R.id.input_food_price);
        foodImageTextView = (TextView) rootView.findViewById(R.id.tv_food_image);

        addFood.setOnClickListener(addFoodButtonOnClickListener);
        addFoodImage.setOnClickListener(addFoodImageOnClickListener);

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getActionBar().setTitle(getResources().getString(R.string.title_menu));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private OnClickListener addFoodButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {

            String foodName = foodNameEditText.getText().toString();
            String foodPrice = foodPriceEditText.getText().toString();
            String restaurantID = sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_ID, null);

            if (foodName.isEmpty() || foodPrice.isEmpty()) {
                Toast.makeText(getContext(), getResources().getString(R.string.error_missing_food_name_price), Toast.LENGTH_SHORT).show();
            } else {
                if (StringUtil.isPriceFormat(foodPrice)) {
                    foodPrice = StringUtil.removeAlphabetLeadingZero(foodPrice);
                    postFoodItem task = new postFoodItem();
                    if (!hasImage) {
                        task.execute(Constants.POST_FOOD_ITEM_URL, foodName, foodPrice, null, restaurantID);
                    } else {
                        ImageUtil imageUtil = new ImageUtil();
                        Bitmap foodPhoto = ((BitmapDrawable) addFoodImage.getDrawable()).getBitmap();
                        String foodPhotoBase64 = imageUtil.encodeToBase64(foodPhoto);
                        task.execute(Constants.POST_FOOD_ITEM_URL, foodName, foodPrice, foodPhotoBase64, restaurantID);
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_invalid_price), Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private OnClickListener addFoodImageOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            openImageIntent();
        }
    };

    // Open dialog to select camera or gallery
    private void openImageIntent() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle(getResources().getString(R.string.title_upload_image))
                .setSingleChoiceItems(R.array.upload_actions, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Intent galleryIntent = new Intent();
                                galleryIntent.setType("image/*");
                                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(galleryIntent, "Select Picture"), GALLERY_REQUEST);
                                break;
                            case 1:
                                // Camera.
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                                break;
                        }
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap photo;
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            photo = (Bitmap) data.getExtras().get("data");
            photo = ThumbnailUtils.extractThumbnail(photo, 200, 200, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
            addFoodImage.setImageBitmap(photo);
        } else if (requestCode == GALLERY_REQUEST && resultCode == Activity.RESULT_OK) {
            Uri selectedImageUri = data.getData();
            try {
                photo = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), selectedImageUri);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotatedPhoto = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix, true);
                rotatedPhoto = ThumbnailUtils.extractThumbnail(rotatedPhoto, 200, 200, ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
                addFoodImage.setImageBitmap(rotatedPhoto);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        hasImage = true;
        foodImageTextView.setVisibility(View.GONE);
    }

    private class postFoodItem extends AsyncTask<String, Void, Void> {

        JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_FOOD_NAME, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_FOOD_PRICE, strings[2]));
            params.add(new BasicNameValuePair(Constants.TAG_FOOD_PHOTO, strings[3]));
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[4]));
            jsonObjectAddFood = jsonParser.makeHttpRequest(strings[0], "POST", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectAddFood.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_add_food), Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    Toast.makeText(getContext(), getResources().getString(R.string.success_add_food), Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_missing_internet_connection), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
