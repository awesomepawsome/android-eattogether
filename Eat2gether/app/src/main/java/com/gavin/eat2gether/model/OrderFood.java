package com.gavin.eat2gether.model;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by gavin on 26/07/2016.
 */
public class OrderFood implements Serializable {

    private int idFood;
    private int quantity;

    public OrderFood(int idFood, int quantity) {
        this.idFood = idFood;
        this.quantity = quantity;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
