package com.gavin.eat2gether.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.adapter.ViewPagerAdapter;

/**
 * Created by gavin on 24/07/2016.
 */
public class CustomerHomeFragment extends Fragment {

    TabLayout tabLayout;
    ViewPager viewPager;
    View view;
    ViewPagerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_customer_main, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(3);

        getActivity().getActionBar().setTitle(getActivity().getResources().getString(R.string.app_name));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        String restaurant = getResources().getString(R.string.title_restaurant);
        String event = getResources().getString(R.string.title_event);
        String notification = getResources().getString(R.string.title_notification);

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new RestaurantFragment(), restaurant);
        adapter.addFragment(new EventsFragment(), event);
        adapter.addFragment(new Notifications(), notification);
        viewPager.setAdapter(adapter);

        //     viewPager.setAdapter(new SimpleFragmentStatePagerAdapter(getFragmentManager(),mParentString)
        //  viewPager.setAdapter(new SimpleFragmentStatePagerAdapter(getChildFragmentManager(),mParentString))
    }

}