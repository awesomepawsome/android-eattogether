package com.gavin.eat2gether.util;

public class Constants {

    // For logging purpose
    public static final String APP_NAME = "EAT_TOGETHER";

    // Intent extras
    public static final String INTENT_EXTRAS_LOGIN_ROLE = "LOGIN_ROLE";

    // Shared preferences
    public static final String SHARED_PREFERENCES_TAG = "SHARED_PREFERENCES_MAIN";
    public static final String SHARED_PREFERENCES_EMAIL = "LOGIN_EMAIL";
    public static final String SHARED_PREFERENCES_DISPLAY_NAME = "PREF_DISPLAY_NAME";
    public static final String SHARED_PREFERENCES_RESTAURANT_ID = "RESTAURANT_ID";
    public static final String SHARED_PREFERENCES_RESTAURANT_NAME = "RESTAURANT_NAME";
    public static final String SHARED_PREFERENCES_RESTAURANT_TYPE = "RESTAURANT_TYPE";
    public static final String SHARED_PREFERENCES_RESTAURANT_WEBSITE = "RESTAURANT_WEBSITE";
    public static final String SHARED_PREFERENCES_RESTAURANT_PHONE = "RESTAURANT_PHONE";
    public static final String SHARED_PREFERENCES_RESTAURANT_ADDRESS = "RESTAURANT_ADDRESS";
    public static final String SHARED_PREFERENCES_RESTAURANT_OPERATING_HOURS = "RESTAURANT_OPERATING_HOURS";
    public static final String SHARED_PREFERENCES_MENU_NAME = "MENU_NAME";
    public static final String SHARED_PREFERENCES_MENU_PRICE = "MENU_PRICE";
    public static final String SHARED_PREFERENCES_MENU_PHOTO = "MENU_PHOTO";
    public static final String SHARED_PREFERENCES_MENU_ID = "MENU_ID";
    public static final String SHARED_PREFERENCES_FIREBASE_ID = "FIREBASE_ID";

    // IP
    public static final String IP_ADDRESS = "192.168.0.3";

    // Project folder name
    public static final String FOLDER_NAME = "php_mysql";

    // Location
    public static double mLongitude = 0.0;
    public static double mLatitude = 0.0;

    // Common get web service
    public static final String GET_FOOD_MENU_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/get_food_menu.php";

    // Owner get web services
    public static final String GET_OWNER_LOGIN_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/owner/get_owner_login_profile.php";
    public static final String GET_OWNER_RESTAURANT_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/owner/get_owner_restaurant_profile.php";
    public static final String GET_TODAY_RESERVATION_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/owner/get_today_reservation.php";
    public static final String GET_DATE_RESERVATION_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/owner/get_date_reservation.php";
    public static final String GET_RESERVATION_ORDER_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/owner/get_reservation_order.php";

    // Owner post web services
    public static final String POST_FOOD_ITEM_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/owner/insert_food_menu.php";
    public static final String UPDATE_FOOD_ITEM_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/owner/update_food_menu.php";

    // Customer get web services
    public static final String GET_CUSTOMER_LOGIN_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_customer_login_profile.php";
    public static final String GET_NEAREST_RESTAURANT = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_nearest_restaurant.php";
    public static final String GET_RESTAURANT_INFO = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_restaurant_details.php";
    public static final String GET_TABLE_AVAILABILITY = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_table_count.php";
    public static final String GET_EVENT_ATTENDEES = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_attendees.php";
    public static final String GET_CUSTOMER_UPCOMING_EVENTS = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_upcoming_reservation.php";
    public static final String GET_CUSTOMER_ORDERS = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_orders.php";
    public static final String GET_NOTIFICATION_INVITATIONS = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_notification_invitation.php";
    public static final String GET_ACCEPTED_ATTENDEES = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_accepted_attendee.php";
    public static final String GET_VOTING_RESULT = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_voting_result.php";
    public static final String GET_EVENT_DETAILS = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/get_reservation_details.php";

    // Customer post web services
    public static final String POST_CUSTOMER_REG_URL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_customer.php";
    public static final String POST_CUSTOMER_TEMP_RESERVE_ORGANIZER = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_invitation_organizer.php";
    public static final String POST_CUSTOMER_TEMP_RESERVE_INVITED = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_invitation_invited.php";

    public static final String POST_CUSTOMER_VOTE_RESERVE_ORGANIZER = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_voting_organizer.php";
    public static final String POST_CUSTOMER_VOTE_RESERVE_INVITED = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_voting_invited.php";

    public static final String POST_CUSTOMER_INVITE_REJECTED = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/update_reject_invitation.php";
    public static final String POST_CUSTOMER_VOTE_REJECTED = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/update_reject_voting.php";
    
    public static final String POST_CUSTOMER_RESERVE = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_reservation.php";
    public static final String POST_CUSTOMER_RESERVE_ORDER = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_reservation_order.php";

    public static final String POST_CUSTOMER_INVITATION = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/update_attendance.php";
    public static final String POST_CUSTOMER_VOTING = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/update_voting.php";

    public static final String POST_CUSTOMER_EMAIL = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/send_email.php";

    public static final String POST_CUSTOMER_FIREBASE_TOKEN = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/insert_new_firebase_token.php";

    // Post push notification
    public static final String POST_NOTIFICATION = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/push_notification.php";
    public static final String POST_NOTIFICATION_BOOKING = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/push_notification_booking.php";
    public static final String POST_NOTIFY_ORGANIZER = "http://" + IP_ADDRESS + "/" + FOLDER_NAME + "/customer/notify_organizer_accepted.php";

    // Serializable tag
    public static final String RESTAURANT_OBJECT = "RESTAURANT";
    public static final String EVENT_OBJECT = "EVENT";

    // JSON tags
    public static final String TAG_SUCCESS = "success";
    public static final String TAG_PROFILE = "profile";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_PASSWORD = "password";
    public static final String TAG_RESTAURANT_ID = "idRestaurant";
    public static final String TAG_RESTAURANT_NAME = "restaurantName";
    public static final String TAG_RESTAURANT_TYPE = "type";
    public static final String TAG_RESTAURANT_WEBSITE = "website";
    public static final String TAG_RESTAURANT_PHONE = "phone";
    public static final String TAG_RESTAURANT_ADDRESS = "address";
    public static final String TAG_RESTAURANT_OPERATING_HOURS = "operatingHour";
    public static final String TAG_RESTAURANT_DISTANCE = "distance";
    public static final String TAG_RESTAURANT_PHOTO = "photo";
    public static final String TAG_FOOD_ID = "idFood";
    public static final String TAG_FOOD_RESULT = "result";
    public static final String TAG_FOOD_NAME = "foodName";
    public static final String TAG_FOOD_PRICE = "price";
    public static final String TAG_FOOD_PHOTO = "photo";
    public static final String TAG_FOOD_QUANTITY = "quantity";
    public static final String TAG_RESERVATION_RESULTS = "results";
    public static final String TAG_RESERVATION_ID = "idReservation";
    public static final String TAG_RESERVATION_PAX = "numberOfPax";
    public static final String TAG_RESERVATION_DATE = "reserve";
    public static final String TAG_RESERVATION_TIME = "time";
    public static final String TAG_RESERVATION_NAME = "displayName";
    public static final String TAG_RESERVATION_PHONE = "phone";
    public static final String TAG_RESERVATION_EMAIL = "Login_email";
    public static final String TAG_DISPLAY_NAME = "displayName";
    public static final String TAG_USER_PHONE = "phone";
    public static final String TAG_MESSAGE = "message";
    public static final String TAG_RESULT = "result";
    public static final String TAG_EVENT_ID = "idReservation";
    public static final String TAG_EVENT_DATETIME = "reserveDateTime";
    public static final String TAG_EVENT_PAX = "numberOfPax";
    public static final String TAG_EVENT_RESTAURANT = "idRestaurant";
    public static final String TAG_ATTENDEE_EMAIL = "customerAttendee";
    public static final String TAG_NOTIFICATION_INVITATION = "invitation";
    public static final String TAG_EVENT_VALIDITY = "validity";
    public static final String TAG_ACCEPTED_ATTENDEE = "customerInvited";

    public static final String TAG_FIREBASE_UNIQUE_ID = "firebaseId";

    public static final String TAG_LATITUDE = "fLatitude";
    public static final String TAG_LONGTITUDE = "fLongitude";

    // temporary and voting reservation
    public static final String TAG_TEMP_DATE_TIME = "tempDateTime";
    public static final String TAG_VOTE_DATE_TIME = "voteDateTime";
    public static final String TAG_ORGANIZER_EMAIL = "organizer_email";
    public static final String TAG_INVITED_EMAIL = "invited_email";
    public static final String TAG_CUSTOMER_ORGANIZER = "Customer_organizer";
    public static final String TAG_ID_VOTE_RESTAURANT = "idVoteRestaurant";

    public static final String TAG_APPEND_RESTAURANT = "appendIdRestaurant";

    public static final String TAG_NOTI_TYPE = "type";
    // update temporary reservation result
    public static final String TAG_ATTEND = "isAttend";
    public static final String TAG_TEMP_RESERVE = "idTempReserve";
    public static final String TAG_RESTAURANT_SELECTION = "restaurantSelection";
    public static final String TAG_RESTAURANT_COUNT = "occurrence";
    public static final String TAG_VOTE_RESERVE = "idVoteRestaurant";
    public static final String TAG_RESPONDER_EMAIL = "responder_email";
    public static final String TAG_RESPOND_STATUS = "status";

    public static final String TAG_NEW_ORDER = "newOrder";
    public static final String TAG_NEW_VOTE = "newVote";

    // Firebase
    public static final String FIREBASE_APP = "https://eat2gether-fec14.firebaseio.com";
    public static final String FIREBASE_ID = "eat2gether-fec14";

}
