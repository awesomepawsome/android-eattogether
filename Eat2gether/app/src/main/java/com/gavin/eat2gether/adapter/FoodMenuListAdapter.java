package com.gavin.eat2gether.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.ImageUtil;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by gavin on 7/22/2016.
 */
public class FoodMenuListAdapter extends BaseAdapter {

    Context context;
    List<String> dataId, dataName, dataPrice, dataPhoto;

    private static LayoutInflater inflater = null;
    private ImageUtil imageUtil = new ImageUtil();
    private DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public FoodMenuListAdapter(Context context, List<String> dataId, List<String> dataName, List<String> dataPrice, List<String> dataPhoto) {
        this.context = context;
        this.dataId = dataId;
        this.dataName = dataName;
        this.dataPrice = dataPrice;
        this.dataPhoto = dataPhoto;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataName.size();
    }

    @Override
    public Object getItem(int i) {
        return dataName.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        if (v == null)
            v = inflater.inflate(R.layout.item_restaurant_menu, null);
        TextView foodNameTextView = (TextView) v.findViewById(R.id.tv_food_name);
        TextView foodPriceTextView = (TextView) v.findViewById(R.id.tv_food_price);
        ImageView foodPhotoImageView = (ImageView) v.findViewById(R.id.img_food);

        foodNameTextView.setText(dataName.get(i));
        foodNameTextView.setTag(dataId.get(i));

        Double priceDouble = Double.parseDouble(dataPrice.get(i));
        String formattedPrice = "RM" + decimalFormat.format(priceDouble);
        foodPriceTextView.setText(formattedPrice);

        if (!dataPhoto.get(i).contains("null")) {
            foodPhotoImageView.setImageBitmap(imageUtil.decodeBase64(dataPhoto.get(i)));
        } else {
            foodPhotoImageView.setImageResource(R.mipmap.ic_launcher);
            foodPhotoImageView.setTag(R.mipmap.ic_launcher);
        }
        return v;
    }
}
