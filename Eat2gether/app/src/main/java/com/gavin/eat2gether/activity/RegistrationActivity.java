package com.gavin.eat2gether.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.StringUtil;
import com.gavin.eat2gether.webservice.JSONParser;
import com.google.firebase.iid.FirebaseInstanceId;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RegistrationActivity extends AppCompatActivity {

    EditText inputName, inputEmail, inputPhone, inputPassword, inputConfirmPassword;
    private ProgressDialog progressDialog;
    private JSONObject jsonObjectRegistration;
    private SharedPreferences sharedPreferences;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
        initView();
    }

    private void initView() {
        inputName = (EditText) findViewById(R.id.input_new_name);
        inputEmail = (EditText) findViewById(R.id.input_new_email);
        inputPhone = (EditText) findViewById(R.id.input_new_phone);
        inputPassword = (EditText) findViewById(R.id.input_password);
        inputConfirmPassword = (EditText) findViewById(R.id.input_confirm_password);
        Button btnSignup = (Button) findViewById(R.id.button_signup);
        btnSignup.setOnClickListener(signupButtonOnClickListener);
        ImageView closeButton = (ImageView) findViewById(R.id.button_close);
        closeButton.setOnClickListener(closeButtonOnClickListener);
    }

    private OnClickListener signupButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            int validationMessageRes = validateInput();

            if (validationMessageRes != 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(validationMessageRes),
                        Toast.LENGTH_SHORT).show();
            } else {

                sharedPreferences.edit().putString("firebaseEmail", inputEmail.getText().toString()).apply();
                sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_DISPLAY_NAME, inputName.getText().toString()).apply();

                // check registration email with server
                token = sharedPreferences.getString("token", null);
//                String storedToken = sharedPreferences.getString("storedToken", null);
                if (token == null) {
                    Thread runnable = new Thread() {
                        public void run() {
                            try {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    };
                    runnable.start();
//                    DeleteToken task = new DeleteToken();
//                    task.execute();
                    String newToken = "";
                    do {
                        newToken = FirebaseInstanceId.getInstance().getToken();
                        Log.d("MyFirebaseIDService", "Refreshed new token: " + newToken);
                    } while (newToken == null);

                    PostUserData task = new PostUserData();
                    task.execute(Constants.POST_CUSTOMER_REG_URL, inputEmail.getText().toString().trim(),
                            inputPassword.getText().toString(), inputName.getText().toString(), inputPhone.getText().toString(), newToken);

//                    sharedPreferences.edit().putString("storedToken", token).apply();
                } else {
                    PostUserData task = new PostUserData();
                    task.execute(Constants.POST_CUSTOMER_REG_URL, inputEmail.getText().toString().trim(),
                            inputPassword.getText().toString(), inputName.getText().toString(), inputPhone.getText().toString(), token);
//                    sharedPreferences.edit().putString("storedToken", null).apply();
                }
            }
        }
    };

    private int validateInput() {

        String password = inputPassword.getText().toString();
        String confirmPassword = inputConfirmPassword.getText().toString();

        boolean isInputNameFilled = !StringUtil.isNullOrEmpty(inputName.getText().toString());
        boolean isInputEmailFilled = !StringUtil.isNullOrEmpty(inputEmail.getText().toString());
        boolean isInputPhoneFilled = !StringUtil.isNullOrEmpty(inputPhone.getText().toString());
        boolean isInputPasswordFilled = !StringUtil.isNullOrEmpty(inputName.getText().toString());
        boolean isInputConfirmPasswordFilled = !StringUtil.isNullOrEmpty(inputName.getText().toString());
        boolean isInputEmailValid = StringUtil.isValidEmail(inputEmail.getText().toString().trim());

        boolean isFilled = isInputNameFilled && isInputEmailFilled && isInputPhoneFilled
                && isInputPasswordFilled && isInputConfirmPasswordFilled;

        if (!isFilled) {
            return R.string.error_empty_field;
        } else if (!isInputEmailValid) {
            return R.string.error_email_format;
        } else if (!password.equals(confirmPassword)) {
            return R.string.error_invalid_password;
        } else {
            // 0 indicate valid registration
            return 0;
        }

    }

    private OnClickListener closeButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            onBackPressed();
        }
    };

    private class PostUserData extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RegistrationActivity.this);
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair(Constants.TAG_EMAIL, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_PASSWORD, strings[2]));
            params.add(new BasicNameValuePair(Constants.TAG_DISPLAY_NAME, strings[3]));
            params.add(new BasicNameValuePair(Constants.TAG_USER_PHONE, strings[4]));
            params.add(new BasicNameValuePair(Constants.TAG_FIREBASE_UNIQUE_ID, strings[5]));
            jsonObjectRegistration = jsonParser.makeHttpRequest(strings[0], "POST", params);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectRegistration.getInt(Constants.TAG_SUCCESS);
                String message = jsonObjectRegistration.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    finish();
                    Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
                    intent.putExtra(Constants.INTENT_EXTRAS_LOGIN_ROLE, 0);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

            sharedPreferences.edit().putString(Constants.SHARED_PREFERENCES_EMAIL, inputEmail.getText().toString().trim()).apply();
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class DeleteToken extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(RegistrationActivity.this);
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            do {
                token = FirebaseInstanceId.getInstance().getToken();
            } while (token == null);

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
