package com.gavin.eat2gether.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.adapter.FoodListAdapter;
import com.gavin.eat2gether.model.Food;
import com.gavin.eat2gether.model.OrderFood;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class FoodOrderFragment extends Fragment {

    /**********
     * Paypal
     **********/
    private static final String TAG = "paymentExample";
    /**
     * - Set to PayPalConfiguration.ENVIRONMENT_PRODUCTION to move real money.
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     * <p/>
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;

    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "credentials from developer.paypal.com";

    private static final int REQUEST_CODE_PAYMENT = 1;

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);
    /****************************/

    String idRestaurant, restaurantName, dateTime, organizerEmail;
    int numberOfPax,eventId;
    boolean isNewOrder, isVote = false;

    ListView foodListView;
    TextView emptyMenuTextView;
    Button confirmButton;

    static TextView priceTextView;

    FoodListAdapter adapter;
    List<Food> foods = new ArrayList<Food>();
    static List<OrderFood> orderFoods = new ArrayList<OrderFood>();

    private ProgressDialog progressDialog;

    public FoodOrderFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Bundle args = getArguments();
            idRestaurant = args.getString(Constants.TAG_RESTAURANT_ID);
            restaurantName = args.getString(Constants.TAG_RESTAURANT_NAME);
            dateTime = args.getString(Constants.TAG_EVENT_DATETIME);
            organizerEmail = args.getString(Constants.TAG_ORGANIZER_EMAIL);
            numberOfPax = args.getInt(Constants.TAG_EVENT_PAX);
            System.out.println("num of pax " + numberOfPax);
            eventId = args.getInt(Constants.TAG_EVENT_ID);
            isNewOrder = args.getBoolean(Constants.TAG_NEW_ORDER);
            isVote = args.getBoolean(Constants.TAG_NEW_VOTE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(getContext(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getContext().startService(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_order, container, false);

        initView(rootView);

        getActivity().getActionBar().setTitle(getResources().getString(R.string.label_restaurant));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        FoodHttp task = new FoodHttp();
        String restaurantId = idRestaurant;
        if (restaurantId != null) {
            task.execute(Constants.GET_FOOD_MENU_URL, restaurantId);
        }

        return rootView;
    }

    private void initView(View rootView) {
        foodListView = (ListView) rootView.findViewById(R.id.list_food_menu);
        emptyMenuTextView = (TextView) rootView.findViewById(R.id.tv_empty_menu);
        priceTextView = (TextView) rootView.findViewById(R.id.tv_price);
        confirmButton = (Button) rootView.findViewById(R.id.button_confirm_order);
        confirmButton.setOnClickListener(confirmButtonOnClickListener);
        orderFoods.clear();
    }

    private View.OnClickListener confirmButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String totalPrice = priceTextView.getText().toString();
            if (!totalPrice.equals("RM 0.00")) {
                // go to payment page
                PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);

                Intent intent = new Intent(getContext(), PaymentActivity.class);

                // send the same configuration for restart resiliency
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
                startActivityForResult(intent, REQUEST_CODE_PAYMENT);
            } else {
                Toast.makeText(getContext(), getResources().getString(R.string.error_submission),
                        Toast.LENGTH_SHORT).show();
            }

        }
    };

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(priceTextView.getText().toString().substring(3)), "MYR", "Total",
                paymentIntent);
    }

    public static void updateOrderedFood(int idFood, int quantity, double amount) {
        double currentAmount = Double.parseDouble(priceTextView.getText().toString().substring(3));
        double newAmount = currentAmount + amount;
        // round to 2 decimals
        DecimalFormat decimalFormat = new DecimalFormat("####0.00");
        decimalFormat.setRoundingMode(RoundingMode.CEILING);
        priceTextView.setText("RM " + decimalFormat.format(newAmount));

        boolean isFound = false;
        for (int i = 0; i < orderFoods.size(); i++) {
            if (orderFoods.get(i).getIdFood() == idFood) {
                orderFoods.get(i).setQuantity(quantity);
                isFound = true;
                break;
            }
        }
        if (!isFound) {
            orderFoods.add(new OrderFood(idFood, quantity));
        }
    }

    private class FoodHttp extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectFood;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            jsonObjectFood = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // clear list view if navigate from other tabs
            if (adapter != null) {
                foods.clear();
                adapter.notifyDataSetChanged();
            }

            JSONObject foodAttributes;

            try {
                int success = jsonObjectFood.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    // No nearby restaurant
                } else if (success == 1) {
                    JSONArray foodItems = jsonObjectFood.getJSONArray(Constants.TAG_FOOD_RESULT);
                    for (int i = 0; i < foodItems.length(); i++) {
                        foodAttributes = foodItems.getJSONObject(i);

                        int idFood = Integer.parseInt(foodAttributes.getString(Constants.TAG_FOOD_ID));
                        String name = foodAttributes.getString(Constants.TAG_FOOD_NAME);
                        double price = Double.parseDouble(foodAttributes.getString(Constants.TAG_FOOD_PRICE));
                        String photo = foodAttributes.getString(Constants.TAG_FOOD_PHOTO);
                        int idRestaurant = Integer.parseInt(foodAttributes.getString(Constants.TAG_RESTAURANT_ID));

                        foods.add(new Food(idFood, name, price, photo, idRestaurant));
                    }

                    adapter = new FoodListAdapter(getContext(), getFragmentManager(), foods);
                    foodListView.setAdapter(adapter);

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class ReservationHttp extends AsyncTask<String[], Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject emailJsonObject, orderJsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Submitting. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String[]... strings) {
            String[] passed = strings[0]; // get passed array

            if(isNewOrder) {
                List<NameValuePair> paramsOrganizer = new ArrayList<NameValuePair>();
                paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_EVENT_DATETIME, passed[1]));
                paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, passed[2]));
                paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_ORGANIZER_EMAIL, passed[3]));
                paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_EVENT_PAX, passed[4]));
                paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_EVENT_ID, passed[5]));
                paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_NEW_VOTE, passed[6]));
                jsonParser.makeHttpRequest(passed[0], "POST", paramsOrganizer);

                // send email to organizer only for confirmed reservation
                List<NameValuePair> paramsEmail = new ArrayList<NameValuePair>();
                paramsEmail.add(new BasicNameValuePair(Constants.TAG_EVENT_DATETIME, passed[1]));
                paramsEmail.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_NAME, restaurantName));
                paramsEmail.add(new BasicNameValuePair(Constants.TAG_ORGANIZER_EMAIL, passed[3]));
                paramsEmail.add(new BasicNameValuePair(Constants.TAG_EVENT_PAX, passed[4]));
                emailJsonObject = jsonParser.makeHttpRequest(Constants.POST_CUSTOMER_EMAIL, "POST", paramsEmail);

                for (int j= 0; j<passed.length; j++) {
                    System.out.println("Param "  + j + " " + passed[j]);
                }
            }
            List<NameValuePair> paramsOrder = new ArrayList<NameValuePair>();
            paramsOrder.add(new BasicNameValuePair(Constants.TAG_EMAIL, passed[3]));
            for (int i = 8; i < passed.length; i++) {
                // odd values represent food id, even values represent quantity
                if (i % 2 == 0) {
                    System.out.println("Param food id: "  + i + " " + passed[i]);
                    paramsOrder.add(new BasicNameValuePair(Constants.TAG_FOOD_ID, passed[i]));
                } else {
                    System.out.println("Param quantity: " + i + " " + passed[i]);
                    paramsOrder.add(new BasicNameValuePair(Constants.TAG_FOOD_QUANTITY, passed[i]));
                    orderJsonObject = jsonParser.makeHttpRequest(passed[7], "POST", paramsOrder);
                    paramsOrder.remove(Constants.TAG_FOOD_ID);
                    paramsOrder.remove(Constants.TAG_FOOD_QUANTITY);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                // order result
                int success = orderJsonObject.getInt(Constants.TAG_SUCCESS);
                String message = orderJsonObject.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    // Clear all back stack and show events tab
                    int backStackCount = getFragmentManager().getBackStackEntryCount();
                    for (int i = 0; i < backStackCount; i++) {
                        // todo: pop to the correct pager
                        // Get the back stack fragment id.
                        int backStackId = getFragmentManager().getBackStackEntryAt(i).getId();
                        getFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }

                // email result for organizer only
                int successEmail = emailJsonObject.getInt(Constants.TAG_SUCCESS);
                String messageEmail = emailJsonObject.getString(Constants.TAG_MESSAGE);
                if (successEmail == 0) {
                    Log.e(Constants.APP_NAME, "Send email to organizer: " + messageEmail);
                } else if (successEmail == 1){
                    Log.d(Constants.APP_NAME, "Send email to organizer: " + messageEmail);
                } else {
                    Toast.makeText(getContext(), "Email function failed, check your connection settings", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirm != null) {

                    try {
                        Log.d(TAG, confirm.toJSONObject().toString(4));
                        Log.d(TAG, confirm.getPayment().toJSONObject().toString(4));

                        Toast.makeText(getContext(), "PaymentConfirmation info received from PayPal", Toast.LENGTH_SHORT).show();

                        String[] reservations = {Constants.POST_CUSTOMER_RESERVE, dateTime, idRestaurant, organizerEmail,
                                String.valueOf(numberOfPax), String.valueOf(eventId), String.valueOf(isVote)};

                        String[] attendeeOrders = new String[1 + (orderFoods.size() * 2)];
                        attendeeOrders[0] = Constants.POST_CUSTOMER_RESERVE_ORDER;
                        int counter = 1;
                        // odd values represent food id
                        int keyFood = 0;
                        // even values represent quantity
                        int keyQuantity = 1;
                        for (int i = 0; i < orderFoods.size(); i++) {
                            attendeeOrders[counter + keyFood] = String.valueOf(orderFoods.get(i).getIdFood());
                            attendeeOrders[counter + keyQuantity] = String.valueOf(orderFoods.get(i).getQuantity());
                            keyFood = keyFood + 1;
                            keyQuantity = keyQuantity + 1;
                            counter++;
                        }

                        // combine reservation web services with arrays of food orders
                        String[] passings = new String[reservations.length + attendeeOrders.length];
                        System.arraycopy(reservations, 0, passings, 0, reservations.length);
                        System.arraycopy(attendeeOrders, 0, passings, reservations.length, attendeeOrders.length);

                        for (int i = 0; i < passings.length; i++) {
                            Log.d(Constants.APP_NAME, "Food Order:" + passings[i]);
                        }

                        ReservationHttp task = new ReservationHttp();
                        task.execute(passings);

                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }

                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    @Override
    public void onDestroy() {
        // Stop service when done
        getContext().stopService(new Intent(getContext(), PayPalService.class));
        super.onDestroy();
    }

}
