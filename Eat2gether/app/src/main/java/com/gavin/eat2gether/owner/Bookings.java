package com.gavin.eat2gether.owner;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.adapter.BookingListAdapter;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by gavin on 7/14/2016.
 */
public class Bookings extends Fragment {

    Calendar myCalendar = Calendar.getInstance();
    private static TextView todayDateTextView;
    private ListView bookingList;
    private static Calendar calendar = Calendar.getInstance();
    private String restaurantId;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_owner_bookings, container, false);

        getActivity().getActionBar().setTitle(getResources().getString(R.string.title_bookings));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);
        restaurantId = sharedPreferences.getString(Constants.SHARED_PREFERENCES_RESTAURANT_ID, null);

        todayDateTextView = (TextView) rootView.findViewById(R.id.tv_bookings_date);
        ImageView datePickerIcon = (ImageView) rootView.findViewById(R.id.img_calendar);
        bookingList = (ListView) rootView.findViewById(R.id.list_restaurant_bookings);

        setTodayDate();

        datePickerIcon.setOnClickListener(datePickerListener);

        return rootView;
    }

    private void setTodayDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        String todayDate = dateFormat.format(calendar.getTime());
        todayDateTextView.setText(todayDate);
        getTodayBooking task = new getTodayBooking();
        if (restaurantId != null) {
            task.execute(Constants.GET_TODAY_RESERVATION_URL, restaurantId);
        }
    }

    private OnClickListener datePickerListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            new DatePickerDialog(getContext(), date,
                    myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, month);
            myCalendar.set(Calendar.DAY_OF_MONTH, day);

            String format = "dd MMMM yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
            todayDateTextView.setText(simpleDateFormat.format(myCalendar.getTime()));

            String sqlFormat = "yyyy-MM-dd";
            SimpleDateFormat sqlSimpleDateFormat = new SimpleDateFormat(sqlFormat);
            getDateBooking task = new getDateBooking();
            if (restaurantId != null) {
                task.execute(Constants.GET_DATE_RESERVATION_URL, restaurantId, sqlSimpleDateFormat.format(myCalendar.getTime()));
            }


        }
    };

    private class getTodayBooking extends AsyncTask<String, Void, Void> {

        private JSONObject jsonObjectBooking;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            jsonObjectBooking = JSONParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            List<String> bookingIdList = new ArrayList<String>();
            List<String> bookingNameList = new ArrayList<String>();
            List<String> bookingPhoneList = new ArrayList<String>();
            List<String> bookingEmailList = new ArrayList<String>();
            List<String> bookingPaxList = new ArrayList<String>();
            List<String> bookingDateList = new ArrayList<String>();
            List<String> bookingTimeList = new ArrayList<String>();
            List<String> bookingFood = new ArrayList<String>();
            List<String> bookingQuantity = new ArrayList<String>();
            JSONObject bookingAttributes;

            try {
                int success = jsonObjectBooking.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    // No booking today
                } else if (success == 1) {
                    JSONArray bookingItems = jsonObjectBooking.getJSONArray(Constants.TAG_RESERVATION_RESULTS);
                    for (int i = 0; i < bookingItems.length(); i++) {
                        bookingAttributes = bookingItems.getJSONObject(i);
                        bookingIdList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_ID));
                        bookingNameList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_NAME));
                        bookingPhoneList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_PHONE));
                        bookingEmailList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_EMAIL));
                        bookingPaxList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_PAX));
                        bookingDateList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_DATE));
                        bookingTimeList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_TIME));
                    }

                    BookingListAdapter adapter = new BookingListAdapter(getContext(), bookingIdList, bookingNameList, bookingPhoneList, bookingEmailList, bookingPaxList, bookingDateList, bookingTimeList);
                    bookingList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();


                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    private class getDateBooking extends AsyncTask<String, Void, Void> {

        private JSONObject jsonObjectBooking;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_EVENT_DATETIME, strings[2]));
            jsonObjectBooking = JSONParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            List<String> bookingIdList = new ArrayList<String>();
            List<String> bookingNameList = new ArrayList<String>();
            List<String> bookingPhoneList = new ArrayList<String>();
            List<String> bookingEmailList = new ArrayList<String>();
            List<String> bookingPaxList = new ArrayList<String>();
            List<String> bookingDateList = new ArrayList<String>();
            List<String> bookingTimeList = new ArrayList<String>();
            List<String> bookingFood = new ArrayList<String>();
            List<String> bookingQuantity = new ArrayList<String>();
            JSONObject bookingAttributes;

            try {
                int success = jsonObjectBooking.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    // No booking today
                } else if (success == 1) {
                    JSONArray bookingItems = jsonObjectBooking.getJSONArray(Constants.TAG_RESERVATION_RESULTS);
                    for (int i = 0; i < bookingItems.length(); i++) {
                        bookingAttributes = bookingItems.getJSONObject(i);
                        bookingIdList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_ID));
                        bookingNameList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_NAME));
                        bookingPhoneList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_PHONE));
                        bookingEmailList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_EMAIL));
                        bookingPaxList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_PAX));
                        bookingDateList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_DATE));
                        bookingTimeList.add(bookingAttributes.getString(Constants.TAG_RESERVATION_TIME));
                    }

                    BookingListAdapter adapter = new BookingListAdapter(getContext(), bookingIdList, bookingNameList, bookingPhoneList, bookingEmailList, bookingPaxList, bookingDateList, bookingTimeList);
                    bookingList.setAdapter(adapter);
                    adapter.notifyDataSetChanged();


                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
