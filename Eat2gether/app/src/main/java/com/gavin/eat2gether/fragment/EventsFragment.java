package com.gavin.eat2gether.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.adapter.EventsAdapter;
import com.gavin.eat2gether.model.Event;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {

    View view;
    ListView eventsListView;
    private TextView eventEmptyText;

    private SharedPreferences sharedPreferences;
    private JSONObject jsonObjectEvent;

    private ProgressDialog progressDialog;
    List<Event> events = new ArrayList<>();
    EventsAdapter adapter;

    public EventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_customer_events, container, false);
        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);

        eventsListView = (ListView) view.findViewById(R.id.list_events);
        eventEmptyText = (TextView) view.findViewById(R.id.tv_empty_events);

        eventsListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Event item = (Event)adapterView.getItemAtPosition(i);
                Bundle args = new Bundle();
                args.putSerializable(Constants.EVENT_OBJECT, item);

                if(item.getRestaurantAddress().equals("")){
                    //Voting
                    Fragment toFragment = new VotingResultFragment();
                    toFragment.setArguments(args);
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment, "VotingFragment")
                            .addToBackStack("VotingFragment").commit();
                }else{
                    //Booking
                    Fragment toFragment = new EventDetailFragment();
                    toFragment.setArguments(args);
                    getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment, "EventsFragment")
                            .addToBackStack("EventsFragment").commit();
                }

            }

        });

        return view;
    }

    private class RetrieveEvents extends AsyncTask<String, String, String> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_EMAIL, strings[1]));
            jsonObjectEvent = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return strings[1];
        }

        @Override
        protected void onPostExecute(String email) {
            super.onPostExecute(email);

            JSONObject eventAttributes;

            // clear list view if navigate from other tabs
            if (adapter != null){
                events.clear();
                adapter.notifyDataSetChanged();
            }

            try {
                int success = jsonObjectEvent.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    eventsListView.setVisibility(View.GONE);
                    eventEmptyText.setVisibility(View.VISIBLE);
                } else if (success == 1) {
                    eventsListView.setVisibility(View.VISIBLE);
                    eventEmptyText.setVisibility(View.GONE);

                    JSONArray eventItems = jsonObjectEvent.getJSONArray(Constants.TAG_RESULT);
                    Log.d(Constants.APP_NAME, "eventItems: " + eventItems.length());

                    for (int i = 0; i < eventItems.length(); i++) {
                        eventAttributes = eventItems.getJSONObject(i);
                        int reservationId = eventAttributes.getInt(Constants.TAG_EVENT_ID);
                        String reserveDateTime = eventAttributes.getString(Constants.TAG_EVENT_DATETIME);
                        int noOfPax = eventAttributes.getInt(Constants.TAG_EVENT_PAX);
                        int restaurantId = eventAttributes.getInt(Constants.TAG_RESTAURANT_ID);
                        String restaurantName = eventAttributes.getString(Constants.TAG_RESTAURANT_NAME);
                        String restaurantType = eventAttributes.getString(Constants.TAG_RESTAURANT_TYPE);
                        String restaurantAddress = eventAttributes.getString(Constants.TAG_RESTAURANT_ADDRESS);
                        String validity = eventAttributes.getString(Constants.TAG_EVENT_VALIDITY);
                        String photo = eventAttributes.getString(Constants.TAG_RESTAURANT_PHOTO);

                        Event event = new Event(reservationId,reserveDateTime,noOfPax,email,restaurantId,
                                restaurantName,restaurantType,restaurantAddress,validity, photo);
                        events.add(event);
                    }

                    adapter = new EventsAdapter(getContext(), events);
                    eventsListView.setAdapter(adapter);

                } else {
                    eventsListView.setVisibility(View.GONE);
                    eventEmptyText.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("Events onResume");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            System.out.println("Events refresh");
            String email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL,null);
            RetrieveEvents task = new RetrieveEvents();
            if(email != null){
                task.execute(Constants.GET_CUSTOMER_UPCOMING_EVENTS,email);
            }
        } else {

        }
    }
}
