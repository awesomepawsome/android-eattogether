package com.gavin.eat2gether.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.model.Event;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 06/08/2016.
 */
public class VotingResultFragment extends Fragment {

    Event event;

    View view;
    TextView eventDate, eventPax, optionOneName, optionOneCount, optionTwoName, optionTwoCount, optionOneId, optionTwoId;
    RelativeLayout optionOneLayout, optionTwoLayout;
    Button bookButton;
    int selected = 0, restaurantCount = 0, pax = 0;
    String[] idRestaurant = new String[2];
    String email, restaurantNameEmail = "";
    private SharedPreferences sharedPreferences;
    //Manually add participants and orders (if ordered)
    private JSONObject jsonObjectVote;

    // by default highest vote result is highlighted
    // by default equal vote result will highlight the first selection
    int countOne = 0;
    int countTwo = 0;

    Bundle args;

    public VotingResultFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Bundle args = getArguments();
            event = (Event) args.getSerializable(Constants.EVENT_OBJECT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_customer_vote_respond, container, false);
        sharedPreferences = getActivity().getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        getActivity().getActionBar().setTitle("Restaurant Voting");
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        initView(view);

        email = sharedPreferences.getString(Constants.SHARED_PREFERENCES_EMAIL,null);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        RetrieveRestaurantDetails retrieveRestaurantDetails = new RetrieveRestaurantDetails();
        if (event.getIdReservation() != 0) {
            retrieveRestaurantDetails.execute(Constants.GET_VOTING_RESULT, String.valueOf(event.getIdReservation()));
        }
    }

    public void initView(View view) {
        eventDate = (TextView) view.findViewById(R.id.tv_vote_time);
        eventPax = (TextView) view.findViewById(R.id.tv_vote_pax);
        optionOneName = (TextView) view.findViewById(R.id.tv_selection_one_name);
        optionOneCount = (TextView) view.findViewById(R.id.tv_selection_one_result);
        optionTwoName = (TextView) view.findViewById(R.id.tv_selection_two_name);
        optionTwoCount = (TextView) view.findViewById(R.id.tv_selection_two_result);
        optionOneId = (TextView) view.findViewById(R.id.tv_selection_one_id);
        optionTwoId = (TextView) view.findViewById(R.id.tv_selection_two_id);

        optionOneLayout = (RelativeLayout) view.findViewById(R.id.layout_selection_one);
        optionTwoLayout = (RelativeLayout) view.findViewById(R.id.layout_selection_two);

        bookButton = (Button) view.findViewById(R.id.btn_book_now);

        eventDate.setText("Date: " + event.getReserveDateTime());

        optionOneLayout.setBackground(getResources().getDrawable(R.drawable.drawable_border));
        optionTwoLayout.setBackground(null);

        optionOneLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionOneLayout.setBackground(getResources().getDrawable(R.drawable.drawable_border));
                optionTwoLayout.setBackground(null);
                selected = Integer.parseInt(optionOneId.getText().toString());
                pax = countOne;
                restaurantNameEmail = optionOneName.getText().toString();
            }
        });

        optionTwoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                optionTwoLayout.setBackground(getResources().getDrawable(R.drawable.drawable_border));
                optionOneLayout.setBackground(null);
                selected = Integer.parseInt(optionTwoId.getText().toString());
                pax = countTwo;
                restaurantNameEmail = optionTwoName.getText().toString();
            }
        });

        bookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(Constants.APP_NAME, "selected: " + selected);
                if (selected != 0) {
                    args = new Bundle();
                    args.putString(Constants.TAG_RESTAURANT_ID, String.valueOf(selected));
                    args.putString(Constants.TAG_RESTAURANT_NAME, restaurantNameEmail);
                    args.putString(Constants.TAG_EVENT_DATETIME, event.getReserveDateTime());
                    args.putString(Constants.TAG_ORGANIZER_EMAIL, email);
                    args.putInt(Constants.TAG_EVENT_PAX, pax + 1);
                    args.putBoolean(Constants.TAG_NEW_ORDER,true);
                    args.putBoolean(Constants.TAG_NEW_VOTE,true);
                    args.putInt(Constants.TAG_EVENT_ID,event.getIdReservation());

                    checkTable(String.valueOf(selected));
                }
            }
        });

    }

    private void checkTable(String selectedRestaurant) {
        String reserveSlot = event.getReserveDateTime() + ":00";
        TablesHttp task = new TablesHttp();
        task.execute(Constants.GET_TABLE_AVAILABILITY, selectedRestaurant, String.valueOf(reserveSlot));
    }

    private class TablesHttp extends AsyncTask<String, Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject jsonObjectTableAvailability;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, strings[1]));
            params.add(new BasicNameValuePair(Constants.TAG_EVENT_DATETIME, strings[2]));
            jsonObjectTableAvailability = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = jsonObjectTableAvailability.getInt(Constants.TAG_SUCCESS);
                String message = jsonObjectTableAvailability.getString(Constants.TAG_MESSAGE);
                if (success == 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_late), Toast.LENGTH_SHORT).show();
                    // notify other voted participant of full house restaurant
                    Log.d(Constants.APP_NAME, "" + event.getIdReservation());
                    NotifyFullHouse notificationTask = new NotifyFullHouse();
                    notificationTask.execute(Constants.POST_CUSTOMER_VOTE_REJECTED, String.valueOf(event.getIdReservation()));
                } else if (success == 1) {
                    Fragment toFragment = new FoodOrderFragment();
                    toFragment.setArguments(args);
                    getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment)
                            .addToBackStack(null).commit();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class NotifyFullHouse extends AsyncTask<String, Void, Void>{
        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> paramsNotification = new ArrayList<NameValuePair>();
            paramsNotification.add(new BasicNameValuePair(Constants.TAG_ID_VOTE_RESTAURANT, strings[1]));
            JSONParser.makeHttpRequest(strings[0], "POST", paramsNotification);
            System.out.println("Params " + strings[0] + " " + strings[1]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // Clear all back stack and show events tab
            int backStackCount = getFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < backStackCount; i++) {
                // todo: pop to the correct pager
                // Get the back stack fragment id.
                int backStackId = getFragmentManager().getBackStackEntryAt(i).getId();
                getFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        }
    }

    private class RetrieveRestaurantDetails extends AsyncTask<String, String, String> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_ID_VOTE_RESTAURANT, strings[1]));
            jsonObjectVote = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return strings[1];
        }

        @Override
        protected void onPostExecute(String email) {
            super.onPostExecute(email);

            JSONObject VotingAttributes;

            try {
                System.out.println("Result " + jsonObjectVote);
                int success = jsonObjectVote.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    // something went wrong
                } else if (success == 1) {

                    JSONArray voteItems = jsonObjectVote.getJSONArray(Constants.TAG_RESULT);

                    VotingAttributes = voteItems.getJSONObject(0);
                    // array of appended restaurant id
                    String restaurantAppended = VotingAttributes.getString(Constants.TAG_APPEND_RESTAURANT);
                    idRestaurant[0] = String.valueOf(restaurantAppended.charAt(0));
                    idRestaurant[1] = String.valueOf(restaurantAppended.charAt(2));

                    // get restaurant name
                    String[] passing = new String[2];
                    passing[0] = Constants.GET_RESTAURANT_INFO;
                    passing[1] = idRestaurant[0];
                    restaurantCount = 0;

                    RestaurantHttp task = new RestaurantHttp();
                    task.execute(passing);
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class RestaurantHttp extends AsyncTask<String[], Void, Void> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject restaurantJsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String[]... strings) {
            String[] passed = strings[0]; // get passed array

            List<NameValuePair> paramsOrganizer = new ArrayList<NameValuePair>();
            paramsOrganizer.add(new BasicNameValuePair(Constants.TAG_RESTAURANT_ID, passed[1]));
            restaurantJsonObject = jsonParser.makeHttpRequest(passed[0], "GET", paramsOrganizer);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try {
                int success = restaurantJsonObject.getInt(Constants.TAG_SUCCESS);

                if (success == 0) {
                    String message = restaurantJsonObject.getString(Constants.TAG_MESSAGE);
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else if (success == 1) {
                    JSONObject foodAttributes;
                    JSONArray foodItems = restaurantJsonObject.getJSONArray(Constants.TAG_RESULT);

                    foodAttributes = foodItems.getJSONObject(0);

                    String name = foodAttributes.getString(Constants.TAG_RESTAURANT_NAME);

                    switch (restaurantCount) {
                        case 0:
                            optionOneName.setText(name);
                            optionOneId.setText(idRestaurant[0]);
                            restaurantCount++;
                            restaurantNameEmail = name;

                            String[] passing = new String[2];
                            passing[0] = Constants.GET_RESTAURANT_INFO;
                            passing[1] = idRestaurant[1];
                            RestaurantHttp task = new RestaurantHttp();
                            task.execute(passing);

                            break;
                        case 1:
                            optionTwoName.setText(name);
                            optionTwoId.setText(idRestaurant[1]);

                            RetrieveVotingDetails retrieveVotingDetails = new RetrieveVotingDetails();
                            if (event.getIdReservation() != 0) {
                                retrieveVotingDetails.execute(Constants.GET_VOTING_RESULT, String.valueOf(event.getIdReservation()));
                            }
                            break;
                    }


                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class RetrieveVotingDetails extends AsyncTask<String, String, String> {

        private JSONParser jsonParser = new JSONParser();
        private JSONObject restaurantJsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_ID_VOTE_RESTAURANT, strings[1]));
            jsonObjectVote = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return strings[1];
        }

        @Override
        protected void onPostExecute(String email) {
            super.onPostExecute(email);

            JSONObject VotingAttributes;

            try {
                System.out.println("Result " + jsonObjectVote);
                int success = jsonObjectVote.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {
                    // something went wrong
                } else if (success == 1) {
                    int totalCount = 0;

                    JSONArray voteItems = jsonObjectVote.getJSONArray(Constants.TAG_RESULT);

                    for (int i = 0; i < voteItems.length(); i++) {
                        VotingAttributes = voteItems.getJSONObject(i);
                        String restaurantId = VotingAttributes.getString(Constants.TAG_RESTAURANT_ID);
                        String restaurantCount = VotingAttributes.getString(Constants.TAG_RESTAURANT_COUNT);

                        if (restaurantCount != null) {
                            totalCount = totalCount + Integer.parseInt(restaurantCount);
                        }

                        if(restaurantId.equals(optionOneId.getText().toString())){
                            optionOneCount.setText(restaurantCount + " vote");
                            countOne = Integer.parseInt(restaurantCount);
                        }else if(restaurantId.equals(optionTwoId.getText().toString())){
                            optionTwoCount.setText(restaurantCount + " vote");
                            countTwo = Integer.parseInt(restaurantCount);
                        }

                    }
                    eventPax.setText("Total invited: " + totalCount + " pax");

                    try {
                        if (countOne >= countTwo) {
                            selected = Integer.parseInt(optionOneId.getText().toString());
                            pax = countOne;
                        } else {
                            selected = Integer.parseInt(optionTwoId.getText().toString());
                            optionTwoLayout.setBackground(getResources().getDrawable(R.drawable.drawable_border));
                            optionOneLayout.setBackground(null);
                            pax = countTwo;
                            restaurantNameEmail = optionTwoName.getText().toString();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (countOne == 0 && countTwo == 0) {
                        // No one voted
                        bookButton.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.error_others), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
