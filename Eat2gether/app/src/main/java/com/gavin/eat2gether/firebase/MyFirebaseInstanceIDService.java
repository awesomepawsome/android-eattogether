package com.gavin.eat2gether.firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by gavin on 8/1/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIDService";
    private SharedPreferences sharedPreferences;

    @Override
    public void onTokenRefresh() {

        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_TAG, Context.MODE_PRIVATE);

        // Get registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Log token
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sharedPreferences.edit().putString("token", refreshedToken).apply();
        String email;

        do {
            email = sharedPreferences.getString("firebaseEmail", null);
        } while (email == null || email.isEmpty());

        Log.d(TAG, "Refreshed email: " + email);

        sendRegistrationToServer(refreshedToken, email);

    }

    public void sendRegistrationToServer(String token, String email) {

        updateToken task = new updateToken();
        task.execute(token, email);

    }

    private class updateToken extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("firebaseId", strings[0]));
            params.add(new BasicNameValuePair("email", strings[1]));
            JSONParser.makeHttpRequest(Constants.POST_CUSTOMER_REG_URL  , "POST", params);
            return null;
        }
    }

}