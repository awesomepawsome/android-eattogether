package com.gavin.eat2gether.fragment;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.model.Restaurant;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.util.ImageUtil;

public class RestaurantDetailFragment extends Fragment {

    int idRestaurant;
    String name, type, address, phone, operatingHour, photo;

    ImageView imageImageView;
    TextView nameTextView, typeTextView, addressTextView, phoneTextView, operatingHourTextView;
    Button bookingButton;

    public RestaurantDetailFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Bundle args = getArguments();
            idRestaurant = args.getInt(Constants.TAG_RESTAURANT_ID);
            name = args.getString(Constants.TAG_RESTAURANT_NAME);
            type = args.getString(Constants.TAG_RESTAURANT_TYPE);
            address = args.getString(Constants.TAG_RESTAURANT_ADDRESS);
            phone = args.getString(Constants.TAG_RESTAURANT_PHONE);
            operatingHour = args.getString(Constants.TAG_RESTAURANT_OPERATING_HOURS);
            photo = args.getString(Constants.TAG_RESTAURANT_PHOTO);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_customer_restaurant_info, container, false);

        initView(rootView);

        getActivity().getActionBar().setTitle(getResources().getString(R.string.label_restaurant));
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);

        return rootView;
    }

    private void initView(View rootView) {
        imageImageView = (ImageView) rootView.findViewById(R.id.img_restaurant);
        nameTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_name);
        typeTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_type);
        addressTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_address);
        phoneTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_phone);
        operatingHourTextView = (TextView) rootView.findViewById(R.id.tv_restaurant_operating_hours);
        bookingButton = (Button) rootView.findViewById(R.id.btn_book_now);
        bookingButton.setOnClickListener(bookingButtonOnClickListener);

        Bitmap photoBitmap = ImageUtil.decodeBase64(photo);
        if (photoBitmap == null) {
            Drawable drawable = getResources().getDrawable(R.mipmap.ic_launcher);
            photoBitmap = ((BitmapDrawable)drawable).getBitmap();
            imageImageView.setImageBitmap(photoBitmap);
        } else {
            imageImageView.setImageBitmap(photoBitmap);
        }

        nameTextView.setText(name);
        typeTextView.setText(type);
        addressTextView.setText(address);
        phoneTextView.setText(phone);
        operatingHourTextView.setText(operatingHour);
    }

    private View.OnClickListener bookingButtonOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Bundle args = new Bundle();
            args.putString(Constants.TAG_RESTAURANT_ID, String.valueOf(idRestaurant));
            args.putString(Constants.TAG_RESTAURANT_NAME, name);
            args.putString(Constants.TAG_RESTAURANT_OPERATING_HOURS, operatingHour);

            Fragment toFragment = new BookingFragment();
            toFragment.setArguments(args);
            getFragmentManager().beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out).replace(R.id.container, toFragment)
                    .addToBackStack(null).commit();

        }
    };

}
