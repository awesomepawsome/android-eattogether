package com.gavin.eat2gether.model;

import android.widget.ImageView;

import java.io.Serializable;

/**
 * Created by gavin on 26/07/2016.
 */
public class Food implements Serializable {

    private int idFood;
    private String name;
    private double price;
    private String photo;
    private int idRestaurant;


    public Food(int idFood, String name, double price, String photo, int idRestaurant) {
        this.idFood = idFood;
        this.name = name;
        this.price = price;
        this.photo = photo;
        this.idRestaurant = idRestaurant;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getIdRestaurant() {
        return idRestaurant;
    }

    public void setIdRestaurant(int idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

}
