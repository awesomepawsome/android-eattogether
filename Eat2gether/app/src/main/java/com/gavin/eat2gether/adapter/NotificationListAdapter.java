package com.gavin.eat2gether.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.gavin.eat2gether.R;
import com.gavin.eat2gether.fragment.EventDetailFragment;
import com.gavin.eat2gether.fragment.NotificationRespondEvent;
import com.gavin.eat2gether.fragment.NotificationRespondVote;
import com.gavin.eat2gether.model.Event;
import com.gavin.eat2gether.util.Constants;
import com.gavin.eat2gether.webservice.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gavin on 8/4/2016.
 */
public class NotificationListAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    TextView restaurantNameTextView;
    TextView dateTimeTextView;
    TextView organizerTextView;
    TextView typeTextView;
    TextView idTempReserveTextView;
    private List<String> idTempReserve, type, dataRestaurant, dataDateTime, dataOrganizer;
    private FragmentManager fragmentManager;
    private SharedPreferences sharedPreferences;
    private JSONObject jsonObjectEvent;
    private ProgressDialog progressDialog;

    public NotificationListAdapter(Context context, List<String> idTempReserve, List<String> type, List<String> dataRestaurant,
                                   List<String> dataDatetTime, List<String> dataOrganizer, FragmentManager fragmentManager) {
        this.context = context;
        this.idTempReserve = idTempReserve;
        this.type = type;
        this.dataRestaurant = dataRestaurant;
        this.dataDateTime = dataDatetTime;
        this.dataOrganizer = dataOrganizer;
        this.fragmentManager = fragmentManager;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataRestaurant.size();
    }

    @Override
    public Object getItem(int i) {
        return dataRestaurant.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = inflater.inflate(R.layout.item_notification_booking_list, null);

        restaurantNameTextView = (TextView) view.findViewById(R.id.tv_notification_name);
        dateTimeTextView = (TextView) view.findViewById(R.id.tv_notification_datetime);
        organizerTextView = (TextView) view.findViewById(R.id.tv_notification_organizer);
        typeTextView = (TextView) view.findViewById(R.id.tv_type);
        idTempReserveTextView = (TextView) view.findViewById(R.id.tv_restaurant_id);

        dateTimeTextView.setText(dataDateTime.get(i));
        organizerTextView.setText(dataOrganizer.get(i));
        typeTextView.setText(type.get(i));
        idTempReserveTextView.setText(idTempReserve.get(i));

        if (type.get(i).equals("i")) {
            //Accept or reject invitation
            restaurantNameTextView.setText(dataRestaurant.get(i));
        } else if (type.get(i).equals("v")) {
            //Vote a restaurant
            restaurantNameTextView.setText("Click to vote");
            restaurantNameTextView.setTag(dataRestaurant.get(i));
        } else if (type.get(i).equals("r")) {
            //Invitation response
            restaurantNameTextView.setText(dataRestaurant.get(i));
            dateTimeTextView.setVisibility(View.GONE);
            if (dataDateTime.get(i).equals("0")) {
                organizerTextView.setText(dataOrganizer.get(i) + " has rejected your invitation.");
            } else {
                organizerTextView.setText(dataOrganizer.get(i) + " has accepted your invitation.");
            }
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle args = new Bundle();
                args.putString(Constants.TAG_TEMP_RESERVE, idTempReserve.get(i));
                args.putString(Constants.TAG_NOTI_TYPE, type.get(i));
                args.putString(Constants.TAG_RESTAURANT_NAME, dataRestaurant.get(i));
                args.putString(Constants.TAG_TEMP_DATE_TIME, dataDateTime.get(i));
                args.putString(Constants.TAG_ORGANIZER_EMAIL, dataOrganizer.get(i));

                if (type.get(i).equals("i")) {
                    Fragment toFragment = new NotificationRespondEvent();
                    toFragment.setArguments(args);
                    fragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out)
                            .replace(R.id.container, toFragment, "NotificationFragment").addToBackStack("NotificationFragment").commit();

                } else if (type.get(i).equals("v")) {
                    Fragment toFragment = new NotificationRespondVote();
                    toFragment.setArguments(args);
                    fragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out)
                            .replace(R.id.container, toFragment, "NotificationFragment").addToBackStack("NotificationFragment").commit();

                } else {
                    // not navigate to any page, clear the Bundle
                    args.clear();
                    Toast.makeText(context, "Please confirm your reservation at Events tab", Toast.LENGTH_SHORT).show();
                }

                // disable click listener for reserve type notification
//                else if (type.get(i).equals("r")) {
//                    RetrieveEvents task = new RetrieveEvents();
//                    if (idTempReserve.get(i) != null) {
//                        task.execute(Constants.GET_EVENT_DETAILS, idTempReserve.get(i));
//                    }
//                }
            }
        });

        return view;
    }

    private class RetrieveEvents extends AsyncTask<String, String, String> {

        private JSONParser jsonParser = new JSONParser();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.TAG_TEMP_RESERVE, strings[1]));
            jsonObjectEvent = jsonParser.makeHttpRequest(strings[0], "GET", params);
            return strings[1];
        }

        @Override
        protected void onPostExecute(String email) {
            super.onPostExecute(email);

            JSONObject eventAttributes;

            try {
                int success = jsonObjectEvent.getInt(Constants.TAG_SUCCESS);
                if (success == 0) {

                } else if (success == 1) {

                    JSONArray eventItems = jsonObjectEvent.getJSONArray(Constants.TAG_RESULT);
                    Event event = null;

                    for (int i = 0; i < eventItems.length(); i++) {
                        eventAttributes = eventItems.getJSONObject(i);
                        int reservationId = eventAttributes.getInt(Constants.TAG_EVENT_ID);
                        String reserveDateTime = eventAttributes.getString(Constants.TAG_EVENT_DATETIME);
                        int noOfPax = eventAttributes.getInt(Constants.TAG_EVENT_PAX);
                        int restaurantId = eventAttributes.getInt(Constants.TAG_RESTAURANT_ID);
                        String restaurantName = eventAttributes.getString(Constants.TAG_RESTAURANT_NAME);
                        String restaurantType = eventAttributes.getString(Constants.TAG_RESTAURANT_TYPE);
                        String restaurantAddress = eventAttributes.getString(Constants.TAG_RESTAURANT_ADDRESS);
                        String validity = eventAttributes.getString(Constants.TAG_EVENT_VALIDITY);
                        String photo = eventAttributes.getString(Constants.TAG_RESTAURANT_PHOTO);

                        event = new Event(reservationId, reserveDateTime, noOfPax, email, restaurantId,
                                restaurantName, restaurantType, restaurantAddress, validity, photo);

                    }
                    Bundle args = new Bundle();
                    args.putSerializable(Constants.EVENT_OBJECT, event);
                    Fragment toFragment = new EventDetailFragment();
                    toFragment.setArguments(args);
                    fragmentManager.beginTransaction().setCustomAnimations(R.anim.fade_in, 0, 0, R.anim.fade_out)
                            .replace(R.id.container, toFragment, "NotificationFragment").
                            addToBackStack("NotificationFragment").commit();

                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
